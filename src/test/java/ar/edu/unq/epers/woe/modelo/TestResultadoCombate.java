package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.combate.ResultadoCombate;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;
import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertEquals;

public class TestResultadoCombate {
    private ResultadoCombate resultadoCombate;
    private Rival personaje, rival;
    private Raza elfa, humano;
    private Clase mago, paladin;


    @Before
    public void setUp() {
        resultadoCombate = new ResultadoCombate();

        personaje = new Personaje(elfa, "Hope", mago);

        rival = new Personaje(humano, "NoOne", paladin);
    }

        @Test
        public void creoUnResultadoCombateConGanadorPersonajeHope(){
            resultadoCombate.setGanador(personaje);

            assertEquals(resultadoCombate.getGanador(), personaje);


        }
}
