package ar.edu.unq.epers.woe.modelo;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import static org.mockito.Mockito.mock;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;


public class TestRaza {

    private Raza raza;

    private Clase mago;

    @Before
    public void setUp() {
        raza= new Raza("vampiro");
        Clase mago = mock(Clase.class);

    }

    @Test
    public void nuevaRazaRecienInicializada(){
    assertEquals("vampiro", raza.getNombre());
    assertEquals(0,raza.getAltura());
    assertEquals(0,raza.getPeso());
    assertEquals(0,raza.getEnergiaInicial());
    assertEquals(null,raza.getUrlFoto());
    assertEquals(0,raza.getCantidadPersonajes());
    assertTrue(raza.getClases().isEmpty());
    }

    @Test
    public void unaRazaConNombreVampiroYClaseMagoTieneDeAltura10_Peso10_EnergiaInicial100SinFotoYconUnPersonajeDeClaseMago() {
        raza.setAltura(10);
        raza.setPeso(10);
        raza.setEnergiaIncial(100);
        raza.addClase(mago);
        raza.crearPersonaje("nano",mago);

        assertTrue(raza.getAltura()== 10);
        assertEquals(10,raza.getPeso());
        assertEquals(100,raza.getEnergiaInicial());
        //assertTrue( raza.getClases().contains(mago));
        assertTrue(raza.getCantidadPersonajes() == 1);

    }



    }
