package ar.edu.unq.epers.woe.servicios.JDBCServiceTest;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.service.data.JDBCDataServiceImplementation;
import ar.edu.unq.epers.woe.backend.service.raza.RazaServiceImplementation;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

public class TestRazaService {
    private Raza raza;
    private RazaServiceImplementation rasaService = new RazaServiceImplementation();
    private JDBCDataServiceImplementation dataservice = new JDBCDataServiceImplementation();
    private Clase mago;
    @Before
    public void setUp(){
    raza = new Raza();

        Set<Clase> clases = new HashSet<Clase>();
        mago = new Clase("mago");
        clases.add(mago);
        raza.setNombre("gargola");
        raza.setAltura(10);
        raza.setPeso(50);
        raza.setClases(clases);
        raza.setUrlFoto("");
        raza.setCantidadPersonajes(0);
        raza.setEnergiaIncial(100);


    }
    @After
    public void tearDown(){
        dataservice.eliminarDatos();

    }

    @Test
    public void creamosUnaRazaDesdeElServicioHaciaLaBD(){
        rasaService.crearRaza(raza);
        assertEquals(raza.getId(),rasaService.getRaza(1).getId());
        assertEquals(raza.getNombre(),rasaService.getRaza(1).getNombre());

    }

    @Test
    public void hayEnTotal1razaEnlaBD(){
        rasaService.crearRaza(raza);
        assertEquals(rasaService.getAllRazas().size(),1);
        assertEquals(rasaService.getAllRazas().get(0).getId(),raza.getId());
    }

    @Test
    public void creamosUnPersonajeDeLarazaGargola(){
        rasaService.crearRaza(raza);

        Personaje nano = rasaService.crearPersonaje(1,"nano",mago);

        assertEquals(nano.getRaza().getId(),raza.getId());
    }

}
