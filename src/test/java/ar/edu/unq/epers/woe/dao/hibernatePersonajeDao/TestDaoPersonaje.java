package ar.edu.unq.epers.woe.dao.hibernatePersonajeDao;

import ar.edu.unq.epers.woe.backend.dao.hibernate.PersonajeDAO;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner.run;
import static org.junit.Assert.assertEquals;

public class TestDaoPersonaje {

    private PersonajeDAO personajeDAO = new PersonajeDAO();
    private Personaje personaje1;
    private Personaje personaje2;
    private Raza elf;
    private Clase mago;

    @Before
    public void setUp() {
        elf = new Raza();
        mago = new Clase();
        personaje1 = new Personaje(elf, "Hope", mago);
        personaje2 = new Personaje(elf, "pepe", mago);

    }

    @After
    public void tearDown(){
        personajeDAO.borrarTodo();
    }


    @Test
    public void seVerificaElNombreDeUnPersonajeRecuperado(){
        Personaje personajeRecuperado = TransactionRunner.run(() -> {
                    personajeDAO.guardar(personaje1);
            return personajeDAO.recuperar( 1);
        });

        assertEquals(personaje1.getNombre(), personajeRecuperado.getNombre());
    }

    @Test
    public void seObtienenTodosLosPersonajesGuardados(){
        List<Personaje> personajes = TransactionRunner.run(() -> {
            personajeDAO.guardar(personaje1);
            personajeDAO.guardar(personaje2);
            return personajeDAO.recuperarTodos();
        });
        assertEquals(2, personajes.size());
    }

}
