package ar.edu.unq.epers.woe.dao.Neo4j;

import ar.edu.unq.epers.woe.backend.dao.Neo4j.LugarDaoNeo4j.LugarDaoNeo4j;
import ar.edu.unq.epers.woe.backend.model.lugar.Gimnasio;
import ar.edu.unq.epers.woe.backend.model.lugar.Tienda;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;
import ar.edu.unq.epers.woe.backend.service.data.JDBCDataServiceImplementation;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.mockito.Mockito.mock;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;



public class TestLugarNeo4JDAO {
    private LugarDaoNeo4j lugarDaoNeo4j = new LugarDaoNeo4j();
    private JDBCDataServiceImplementation servisiosBD = new JDBCDataServiceImplementation();

    @Before
    public void setUp() {
        servisiosBD.eliminarDatosNeo4j();
    }


    @Test
    public void alCrearNuevoLugarSeGuardaBien() {
        Tienda t = new Tienda("honeydukes");
        Rival rivalMock = mock(Rival.class);
        Gimnasio g = new Gimnasio("gladiador",rivalMock);
        lugarDaoNeo4j.create(t);
        lugarDaoNeo4j.create(g);
        assertTrue(lugarDaoNeo4j.existeLugar(t.getNombre()));
        assertTrue(lugarDaoNeo4j.existeLugar(g.getNombre()));
    }

    @Test(expected = RuntimeException.class)
    public void alCrearUnLugarQueYaEstaCreadoOcurreExcepcion() {
        Tienda t = new Tienda("honeydukes");
        lugarDaoNeo4j.create(t);
        lugarDaoNeo4j.create(t);
    }
    @Test
    public void seObtieneNombreDeLugaresConectados() {
        Tienda t = new Tienda("tie1");
        Tienda t1 = new Tienda("tie2");
        Rival rivalMock = mock(Rival.class);
        Gimnasio g = new Gimnasio("gim1",rivalMock);
        Gimnasio g1 = new Gimnasio("gim2",rivalMock);

        String tipoCamino = "terrestre";
        String tipoCamino1 = "maritimo";

        lugarDaoNeo4j.create(t);
        lugarDaoNeo4j.create(t1);
        lugarDaoNeo4j.create(g);
        lugarDaoNeo4j.create(g1);

        lugarDaoNeo4j.crearRuta(t.getNombre(),t1.getNombre(),tipoCamino);
        lugarDaoNeo4j.crearRuta(t.getNombre(), g.getNombre(), tipoCamino);
        lugarDaoNeo4j.crearRuta(t1.getNombre(), g1.getNombre(), tipoCamino1);

        assertTrue(lugarDaoNeo4j.conectados(t.getNombre(),tipoCamino).contains(t1.getNombre()));
        assertTrue(lugarDaoNeo4j.conectados(t.getNombre(), tipoCamino).contains(g.getNombre()));
        assertTrue(lugarDaoNeo4j.conectados(t1.getNombre(), tipoCamino1).contains(g1.getNombre()));
    }

    @Test
    public void alPreguntarSiExisteCaminoEntreDosNodosConectadosRespondeTrue() {
        Tienda t1 = new Tienda("tie1");
        Tienda t2 = new Tienda("tie2");
        Rival rivalMock = mock(Rival.class);
        Gimnasio g1 = new Gimnasio("gim1",rivalMock);
        Gimnasio g2 = new Gimnasio("gim2",rivalMock);
        String tipoCamino = "terrestre";

        lugarDaoNeo4j.create(t1);
        lugarDaoNeo4j.create(t2);
        lugarDaoNeo4j.create(g1);
        lugarDaoNeo4j.create(g2);

        lugarDaoNeo4j.crearRuta(t1.getNombre(), t2.getNombre(), tipoCamino);
        lugarDaoNeo4j.crearRuta(t2.getNombre(), g1.getNombre(), tipoCamino);

        assertTrue(lugarDaoNeo4j.existeRuta(t1.getNombre(), g1.getNombre()));
        assertFalse(lugarDaoNeo4j.existeRuta(t1.getNombre(), g2.getNombre()));
    }

    @Test
    public void alPreguntarCostoDeRutaMasBarataRetornaElDeLaMasBarata() {
        Tienda t1 = new Tienda("tie1");
        Tienda t2 = new Tienda("tie2");

        lugarDaoNeo4j.create(t1);
        lugarDaoNeo4j.create(t2);

        String tipoCaminoBarato = "terrestre";
        String tipoCaminoCaro = "aereo";

        lugarDaoNeo4j.crearRuta(t1.getNombre(), t2.getNombre(), tipoCaminoBarato);
        lugarDaoNeo4j.crearRuta(t1.getNombre(), t2.getNombre(), tipoCaminoCaro);
        assertEquals(lugarDaoNeo4j.costoDelTrayectoMasBarato(t1.getNombre(), t2.getNombre()), (Integer) 1);
    }

    @Test
    public void seObtieneNombreDeLugaresConectadosConUnLugarPorCualquierCamino() {
        Tienda t = new Tienda("tie1");
        Tienda t1 = new Tienda("tie2");
        Rival rivalMock = mock(Rival.class);
        Gimnasio g = new Gimnasio("gim1",rivalMock);
        Gimnasio g1 = new Gimnasio("gim2",rivalMock);
        String tipoCamino = "terrestre";
        String tipoCamino1 = "maritimo";

        lugarDaoNeo4j.create(t);
        lugarDaoNeo4j.create(t1);
        lugarDaoNeo4j.create(g);
        lugarDaoNeo4j.create(g1);

        // 't' se relaciona con 't1', 'g', y 'g1'
        lugarDaoNeo4j.crearRuta(t.getNombre(), t1.getNombre(), tipoCamino);
        lugarDaoNeo4j.crearRuta(t.getNombre(), g.getNombre(), tipoCamino);
        lugarDaoNeo4j.crearRuta(t.getNombre(), g1.getNombre(), tipoCamino1);

        List<String> lugaresConectados = lugarDaoNeo4j.conectadosCon(t.getNombre());

        assertEquals( 3, lugaresConectados.size() );
        assertTrue( lugaresConectados.contains(t1.getNombre()) );
        assertTrue( lugaresConectados.contains(g.getNombre()) );
        assertTrue( lugaresConectados.contains(g1.getNombre()) );
    }

}
