package ar.edu.unq.epers.woe.modelo;


import ar.edu.unq.epers.woe.backend.model.inventario.*;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;

import org.junit.Before;
import org.junit.Test;

import java.util.Set;

import static org.junit.Assert.*;

public class TestInventario {
    Inventario inventario;

    Item itemCabeza1;
    Item itemCabeza2;
    Personaje zaneBradford;
    Raza vampiro;
    Clase claseMago;

    @Before
    public void setUp(){


        itemCabeza1 = new Item("ItemCabeza1", Ubicacion.CABEZA, TipoItem.CASCO, 10, 15);
        itemCabeza2 = new Item("ItemCabeza2", Ubicacion.CABEZA, TipoItem.CASCO, 10, 15);

        inventario = new Inventario();

        zaneBradford= new Personaje(vampiro,"zaneBradford1",claseMago);

    }

    @Test
    public void unInventarioNuevoTieneSeisSlots(){
        assertEquals(6, inventario.getSlots().size());
    }

    @Test
    public void unInventarioNuevoNoTieneNingunItemsEnSusSlots(){
        assertNull(inventario.getSlotDe(Ubicacion.CABEZA).getItem());
        assertNull(inventario.getSlotDe(Ubicacion.TORSO).getItem());
        assertNull(inventario.getSlotDe(Ubicacion.MANO_IZQ).getItem());
        assertNull(inventario.getSlotDe(Ubicacion.MANO_DER).getItem());
        assertNull(inventario.getSlotDe(Ubicacion.PIERNAS).getItem());
        assertNull(inventario.getSlotDe(Ubicacion.PIES).getItem());
    }

    @Test
    public void seObtienenLosSlotsDeUnInventario(){
        assertEquals(6, inventario.getSlots().size());
    }

    @Test
    public void seEquipaUnItemConUbicaciónCabezaEnUnInventarioNuevo(){
        inventario.equipar(itemCabeza1, zaneBradford);
        assertTrue(inventario.getSlotDe(Ubicacion.CABEZA).tieneItem());
    }

    @Test
    public void unInventarioIntercambiaItemsDeUbicacionCabezaDeUnPersonaje(){
        //El intercambio se produce cuando el personaje ya tiene en el slot cabeza,
        //un item y quiere agarrar otro con la misma ubicacion
        inventario.equipar(itemCabeza1, zaneBradford);
        inventario.intercambiarItems(zaneBradford, itemCabeza2, itemCabeza1);
        assertEquals("ItemCabeza2", inventario.getSlotDe(Ubicacion.CABEZA).getItem().getNombre());
    }


    @Test
    public void seAgregaUnItemNuevoAUnInventarioEnUnSlotQueYaTeniaUnItemEnEsaUbicacion() {
        inventario.equipar(itemCabeza1, zaneBradford);
        inventario.addItem(itemCabeza2);
        assertEquals("ItemCabeza2", inventario.getSlotDe(Ubicacion.CABEZA).getItem().getNombre());
    }

}
