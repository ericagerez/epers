package ar.edu.unq.epers.woe.dao.JDBCRazaDaoTest;

import ar.edu.unq.epers.woe.backend.dao.JDBC.JDBCRazaDAOJDBC;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.service.data.JDBCDataServiceImplementation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestRazaDAOJDBC {
    private Raza raza;
    private Set<Clase> clases;
    private JDBCRazaDAOJDBC razaDAO = new JDBCRazaDAOJDBC();
    public JDBCDataServiceImplementation dataservis = new JDBCDataServiceImplementation();
    @Before
    public void setUp(){
        dataservis.crearSetDatosIniciales();
        raza =razaDAO.recuperar(1);
        }




    @After
    public void borrarDatosTabla(){
        dataservis.eliminarDatos();
    }



    @Test
    public void  al_guardar_y_luego_recuperar_se_obtiene_objetos_similares(){
        Raza otraRaza = razaDAO.recuperar(1);

       assertEquals(raza.getNombre(), otraRaza.getNombre());
       assertEquals(raza.getId(),otraRaza.getId());
       assertEquals(raza.getAltura(),otraRaza.getAltura());
       assertEquals(raza.getPeso(),otraRaza.getPeso());
       assertEquals(raza.getEnergiaInicial(),otraRaza.getEnergiaInicial());
       assertEquals(raza.getCantidadPersonajes(), otraRaza.getCantidadPersonajes());
       assertEquals(raza.getClases(),otraRaza.getClases());
       assertEquals(raza.getUrlFoto(),otraRaza.getUrlFoto());

        assertTrue(raza != otraRaza);

    }

    @Test
    public void buscamosRazaGuardadaConIdX(){

        assertTrue(1 == raza.getId());
        assertEquals(raza.getId(),razaDAO.recuperar(1).getId());


    }

    @Test
    public void sebuscanTodasLasRazasDeLaBD_Total1(){
        List<Raza> razas = new ArrayList<>();

        razas = razaDAO.recuperarTodos();

        assertFalse(razas.isEmpty());
        assertTrue(razas.size() == 1);
        assertEquals(raza.getId(),razas.get(0).getId());
    }

}


