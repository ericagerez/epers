package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.Slot;
import ar.edu.unq.epers.woe.backend.model.inventario.TipoItem;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
public class TestSlot {

    Slot slot;
    Item item;
    Item item2;
    @Before
    public void setUp(){
        item = new Item("unItem",Ubicacion.TORSO,TipoItem.ARCO,100,50);
        slot= new Slot(Ubicacion.TORSO);
        item2= new Item("item2",Ubicacion.MANO_IZQ,TipoItem.ESCUDO,150,75);
    }

    @Test
    public void unNuevoSlot(){
        assertEquals(Ubicacion.TORSO,slot.getUbicacion());
        assertTrue( slot.getItem() == null);
    }

    @Test
    public void aUnSlotLeSeteamosUnItemDeLaubbicacionCorrespondiente(){
        slot.setItem(item);
        assertTrue(slot.puedeAgregarAItem(item));
        assertEquals(item,slot.getItem());
    }


    @Test
    public void unSlotDeUbicacionTorsoNoPuedeagregarUnItemConUbicacionManoIzq(){
        assertFalse(slot.puedeAgregarAItem(item2));
    }
}
