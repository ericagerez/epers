package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.raza.AumentoTipoAtributo;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class TestClase {

    Clase clase;
    AumentoTipoAtributo atributo;

    @Before
    public void setUp(){
        clase = new Clase("Mago");
        atributo = new AumentoTipoAtributo(TipoAtributo.VIDA, 2);

    }

    @Test
    public void creoUnaClaseMago() {
        assertEquals("Mago",clase.getNombre());
    }

    @Test
    public void aLaClaseMagoLeAgregoUnAumentoTipoAtributo(){

        clase.addAumentoTipoAtributo(atributo);

        assertEquals(1, clase.getAumentoTipoAtributos().size());
    }

}
