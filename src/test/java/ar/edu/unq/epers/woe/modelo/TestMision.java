package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.inventario.Inventario;
import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.TipoItem;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import ar.edu.unq.epers.woe.backend.model.lugar.Gimnasio;
import ar.edu.unq.epers.woe.backend.model.lugar.Taberna;
import ar.edu.unq.epers.woe.backend.model.lugar.Tienda;
import ar.edu.unq.epers.woe.backend.model.misiones.*;
import ar.edu.unq.epers.woe.backend.model.monstruo.Monstruo;
import ar.edu.unq.epers.woe.backend.model.monstruo.TipoMonstruo;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertTrue;

public class TestMision {

    private GanarCombate requisitoMisionGanarCombateContraPersonaje;
    private IrAUnLugar requisitoMisionIrAUnLugar;
    private RequisitoMisionObtenerItem requisitoMisionObtenerItem;
    private RequisitoMisionVencerMonstruos requisitoMisionVencerMonstruos;

    private RecompensaMonedas recompensa1;
    private RecompensaExperiencia recompensa2;
    private RecompensaItem recompensa3;
    private RecompensaMision recompensa4;

    private Tienda tienda;
    private Gimnasio gimnasio;
    private Item itemRequisito;
    private Inventario inventario;
    private Atributo vidaPersonaje;

    private Mision misionGanarCombateContraPersonaje;
    private Mision misionIrAUnLugar;
    private Mision misionObtenerItem;
    private Mision misionVencerMonstruos;

    private Monstruo monstruoRival1;
    private Monstruo monstruoRival2;
    private Monstruo monstruoRival3;
    private Personaje personajeRival;
    private Personaje personaje;


    @Before
    public void setUp(){

        tienda        = new Tienda("tiendaLinda");
        gimnasio      = new Gimnasio("gim", null);
        itemRequisito = new Item("ItemPiola", Ubicacion.CABEZA, TipoItem.CASCO, 10, 13);

        inventario    = new Inventario();
        vidaPersonaje = new Atributo(TipoAtributo.VIDA,1000);

        personajeRival = new Personaje(null, "personajeRival", null);

        personaje = new Personaje(null, "personaje", null);
        personaje.setLugar(gimnasio);
        personaje.setInventario(inventario);
        personaje.getAtributos().add(vidaPersonaje);

        monstruoRival1 = new Monstruo(10.0, 15.3, 20.6, TipoMonstruo.DRAGON);
        monstruoRival2 = new Monstruo(10.0, 15.3, 20.6, TipoMonstruo.DUENDE);
        monstruoRival3 = new Monstruo(10.0, 15.3, 20.6, TipoMonstruo.LOBO);



        requisitoMisionGanarCombateContraPersonaje = new GanarCombate(5, personajeRival);
        requisitoMisionIrAUnLugar                  = new IrAUnLugar(tienda);
        requisitoMisionObtenerItem                 = new RequisitoMisionObtenerItem(itemRequisito);
        requisitoMisionVencerMonstruos             = new RequisitoMisionVencerMonstruos(1);

        misionGanarCombateContraPersonaje = new Mision("misionGanarCombateContraPersonaje",requisitoMisionGanarCombateContraPersonaje, recompensa1);
        misionIrAUnLugar                  = new Mision("misionIrAUnLugar",requisitoMisionIrAUnLugar, recompensa2);
        misionObtenerItem                 = new Mision("misionObtenerItem",requisitoMisionObtenerItem, recompensa3);
        misionVencerMonstruos             = new Mision("misionVencerMonstruos",requisitoMisionVencerMonstruos, recompensa4);
    }

    @Test
    public void unPersonajeCumpleUnaMisionDeIrAUnLugar(){

        personaje.setLugar(new Taberna());

        personaje.aceptarMision(misionIrAUnLugar);
        personaje.setLugar(tienda);
        assertTrue(requisitoMisionIrAUnLugar.cumplioRequisito(personaje));
    }

    @Test
    public void unPersonajeCumpleUnaMisionObtenerItem(){
        personaje.setLugar(new Tienda());
        personaje.aceptarMision(misionObtenerItem);
        personaje.equiparItemAlInventario(itemRequisito);
        assertTrue(requisitoMisionObtenerItem.cumplioRequisito(personaje));
    }

}
