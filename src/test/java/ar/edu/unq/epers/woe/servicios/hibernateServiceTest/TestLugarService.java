package ar.edu.unq.epers.woe.servicios.hibernateServiceTest;

import ar.edu.unq.epers.woe.backend.dao.Neo4j.LugarDaoNeo4j.LugarDaoNeo4j;
import ar.edu.unq.epers.woe.backend.dao.hibernate.*;
import ar.edu.unq.epers.woe.backend.dao.mongo.EventoDAO;
import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.inventario.*;
import ar.edu.unq.epers.woe.backend.model.lugar.Gimnasio;
import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.lugar.Taberna;
import ar.edu.unq.epers.woe.backend.model.lugar.Tienda;
import ar.edu.unq.epers.woe.backend.model.misiones.*;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.service.data.JDBCDataServiceImplementation;
import ar.edu.unq.epers.woe.backend.service.hibernate.lugar.LugarImplService;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.SessionFactoryProvider;
import ar.edu.unq.epers.woe.backend.service.mongo.FeedServiceImpl;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner.run;


public class TestLugarService {

    private LugarImplService serviceLugar;
    private LugarDAO lugarDAOH;
    private PersonajeDAO personajeDAOH;
    private MisionDAO misionDAOH;
    private ItemDAO itemDAOH;
    private TabernaDAO tabernaDAOH;
    private GimnasioDao gimnasioDaoH;
    private LugarDaoNeo4j lugarDaoNeo4j;
    private EventoDAO eventoDAO;


    private Personaje hope, zane;
    private Atributo fuerzaH;
    private ArrayList<Atributo> atributosHope;
    private ArrayList<Mision> listaDeMisiones, misionesAceptadas,misionesCompletadas,misionesHijas;
    private ArrayList<Item> listaItem,mochila;
    private ArrayList<Slot> slots;
    private Raza elf, humano;
    private Clase chaman, picaro;
    private Taberna taberna;
    private Gimnasio gimnasio;
    private Tienda tienda;
    private Item itemRequisito;
    private Inventario inventario,inventarioHope;

    private RecompensaMonedas recompensa1;
    private RecompensaExperiencia recompensa2;
    private RecompensaItem recompensa3;
    private RecompensaMision recompensa4;

    private GanarCombate requisitoMisionGanarCombateContraPersonaje;
    private IrAUnLugar requisitoMisionIrAUnLugar;
    private RequisitoMisionObtenerItem requisitoMisionObtenerItem;
    private RequisitoMisionVencerMonstruos requisitoMisionVencerMonstruos;

    private Mision misionGanarCombateContraPersonaje;
    private Mision misionIrAUnLugar;
    private Mision misionObtenerItem;
    private Mision misionVencerMonstruos;
    private Mision mision;
    private FeedServiceImpl feedService;

    private PreCondicionParaAceptacionMision preCondicionParaAceptacionMision;

    private Item casco, espada;

   private Slot slotCabeza,slotTorso,slotPiernas,slotPies,slotManoIzq,slotManoDer;

    private JDBCDataServiceImplementation servisiosBD = new JDBCDataServiceImplementation();

    @Before
    public void prepare() {
        lugarDAOH = new LugarDAO();
        personajeDAOH = new PersonajeDAO();
        misionDAOH = new MisionDAO();
        itemDAOH = new ItemDAO();
        tabernaDAOH = new TabernaDAO();
        gimnasioDaoH = new GimnasioDao();
        lugarDaoNeo4j = new LugarDaoNeo4j();
        eventoDAO = new EventoDAO();
        serviceLugar = new LugarImplService(lugarDAOH, personajeDAOH, misionDAOH, itemDAOH, tabernaDAOH, gimnasioDaoH, lugarDaoNeo4j,eventoDAO);

        casco  = new Item("ItemCasco", Ubicacion.CABEZA, TipoItem.CASCO, 50, 13);
        espada = new Item("ItemEspada", Ubicacion.MANO_DER, TipoItem.ESPADA, 60, 13);

        mochila     = new ArrayList<Item>();
        mochila.add(casco);

        slotCabeza  = new Slot(Ubicacion.CABEZA);
        slotTorso   = new Slot(Ubicacion.TORSO);
        slotPiernas = new Slot(Ubicacion.PIERNAS);
        slotPies    = new Slot(Ubicacion.PIES);
        slotManoIzq = new Slot(Ubicacion.MANO_IZQ);
        slotManoDer = new Slot(Ubicacion.MANO_DER);

        slots      = new ArrayList<Slot>();
        slots.add(slotCabeza);
        slots.add(slotTorso);
        slots.add(slotPiernas);
        slots.add(slotPies);
        slots.add(slotManoIzq);
        slots.add(slotManoDer);

        inventario = new Inventario();
        inventario.setSlots(slots);

        inventarioHope= new Inventario();

        listaItem  = new ArrayList<Item>();
        listaItem.add(casco);
        listaItem.add(espada);

        tienda = new Tienda("tiendaLinda");
        tienda.setItemsAVender(listaItem);

        atributosHope     = new ArrayList<Atributo>();
        listaDeMisiones   = new ArrayList<Mision>();
        misionesAceptadas = new ArrayList<Mision>();
        misionesCompletadas = new ArrayList<Mision>();
        misionesHijas = new ArrayList<Mision>();

        preCondicionParaAceptacionMision = new PreCondicionParaAceptacionMision();

        elf    = new Raza("elf");
        humano = new Raza("humano");

        chaman = new Clase("chaman");
        picaro = new Clase("picaro");

        itemRequisito = new Item("ItemPiola", Ubicacion.CABEZA, TipoItem.CASCO, 10, 13);

        zane = new Personaje(humano, "zane", picaro);
        zane.setLugar(tienda);
        zane.setBilletera(1000);
        zane.setInventario(inventario);
        zane.setMochila(mochila);
        serviceLugar.guardarPersonaje(zane);

        recompensa1 = new RecompensaMonedas();
        recompensa2 = new RecompensaExperiencia();
        recompensa3 = new RecompensaItem();
        recompensa4 = new RecompensaMision();

        requisitoMisionGanarCombateContraPersonaje = new GanarCombate(5, zane);
        requisitoMisionIrAUnLugar                  = new IrAUnLugar(tienda);
        requisitoMisionObtenerItem                 = new RequisitoMisionObtenerItem(itemRequisito);
        requisitoMisionVencerMonstruos             = new RequisitoMisionVencerMonstruos(1);

        misionGanarCombateContraPersonaje = new Mision("misionGanarCombateContraPersonaje",requisitoMisionGanarCombateContraPersonaje, recompensa1);
        mision                            = new Mision("mision",requisitoMisionIrAUnLugar, recompensa2);
        misionIrAUnLugar                  = new Mision("m",requisitoMisionIrAUnLugar, recompensa2);
        misionIrAUnLugar.setMisionesHijas(misionesHijas);
        misionIrAUnLugar.getMisionesHijas().add(mision);
        misionIrAUnLugar.setPreCondicionParaAceptacionMision(preCondicionParaAceptacionMision);
        misionObtenerItem                 = new Mision("m",requisitoMisionObtenerItem, recompensa3);
        misionVencerMonstruos             = new Mision("m",requisitoMisionVencerMonstruos, recompensa4);



        gimnasio = new Gimnasio("gimnasio", zane);

        ///le agregamos misiones a la taberna.
        taberna  = new Taberna("taberna");
        taberna.addMision(misionGanarCombateContraPersonaje);
        taberna.addMision(misionIrAUnLugar);
        taberna.addMision(misionObtenerItem);
        taberna.addMision(misionVencerMonstruos);


        fuerzaH = new Atributo(TipoAtributo.FUERZA, 30);
        hope    = new Personaje(elf, "hope", chaman);
        hope.setCombatesGanados(10);
        hope.setAtributos(atributosHope);
        atributosHope.add(fuerzaH);
        hope.setLugar(taberna);
        hope.setNivel(1);
        hope.setInventario(inventarioHope);

        serviceLugar.guardarMisionEnLugar(misionIrAUnLugar);
        serviceLugar.guardarMisionEnLugar(misionGanarCombateContraPersonaje);
        serviceLugar.guardarMisionEnLugar(misionObtenerItem);
        serviceLugar.guardarMisionEnLugar(misionVencerMonstruos);
        serviceLugar.guardarPersonaje(hope);
        serviceLugar.guardarTaberna(taberna);


        serviceLugar.guardarGimnasio(gimnasio);
        servisiosBD.eliminarDatosNeo4j();

    }

    @After
    public void cleanup() {
        SessionFactoryProvider.destroy();
    }

    @Test
    public void testListarMisiones() {
        hope.setLugar(taberna);

        serviceLugar.guardarPersonaje(hope);
        assertEquals(0,hope.getMisionesAceptadas().size());

        assertEquals(taberna,hope.getLugar());
        assertEquals("taberna", taberna.getNombre());
        assertEquals(4,taberna.getMisiones().size());

        List<Mision> misiones = taberna.getMisiones();
        assertEquals(misiones,taberna.listarMisiones(hope));

    }

    @Test
    public void testAceptarMision() {
         Raza humano1 = new Raza("humano");
        Clase picaro1 = new Clase("picaro");
        Mision misionIrAUnLugarM = new Mision("m",requisitoMisionIrAUnLugar, recompensa2);
        Taberna taberna1 = new Taberna("taberna1");
        taberna1.addMision(misionIrAUnLugarM);
        taberna.addMision(misionIrAUnLugarM);
        Personaje personaje = new Personaje(humano1,"harry",picaro1);
        personaje.setLugar(taberna1);
        run(()->{//serviceLugar.guardarMisionEnLugar(misionIrAUnLugar);
        serviceLugar.guardarPersonaje(personaje);
        serviceLugar.guardarTaberna(taberna1);});

            this.serviceLugar.aceptarMision(personaje.getId(), misionIrAUnLugarM.getId());
            assertEquals(taberna1,personaje.getLugar());
            assertTrue(taberna1.getMisiones().contains(misionIrAUnLugarM));
            assertEquals(taberna1.getMisiones(),taberna1.listarMisiones(personaje));
            assertEquals(1,personaje.getMisionesAceptadas().size());


    }


   @Test
   public void alCrearNuevoLugarSeGuardaBien() {
       Tienda t = new Tienda("tie99");
       serviceLugar.crearUbicacion(t);
       Lugar lr = run(() -> { return lugarDAOH.recuperar(t.getId()); });
       assertTrue(lugarDaoNeo4j.existeLugar(t.getNombre()));
       assertEquals(t.getNombre(), lr.getNombre());
   }
    @Test
    public void seObtieneLugaresConectados() {
        Tienda t = new Tienda("tie99");
        Tienda t1 = new Tienda("tie100");
        String tipoCamino = "terrestre";
        serviceLugar.crearUbicacion(t);
        serviceLugar.crearUbicacion(t1);
        serviceLugar.conectar(t.getNombre(), t1.getNombre(), tipoCamino);

        assertEquals( 1, serviceLugar.conectados(t.getId(),tipoCamino).size() );
        assertEquals( "tie100", serviceLugar.conectados(t.getId(),tipoCamino).get(0).getNombre() );

    }
    @Test
    public void alMoverAlPJAUnLugarYPreguntarleSuLugarRetornaElNuevoLugar() {
        Tienda t = new Tienda("tie99");
        Tienda t1 = new Tienda("tie100");
        serviceLugar.crearUbicacion(t);
        serviceLugar.crearUbicacion(t1);
        serviceLugar.conectar(t.getNombre(), t1.getNombre(), "terrestre");
        Personaje personaje = new Personaje(humano, "harry", picaro);
        personaje.agregarABilletera(10);
        personaje.setLugar(t);
        run(() -> {personajeDAOH.guardar(personaje);});

        serviceLugar.mover(personaje.getId(), t1.getId());
        Personaje pjr = run(() -> {return personajeDAOH.recuperar(personaje.getId());});

        assertEquals(pjr.getLugar().getClass(), Tienda.class);
        assertEquals(pjr.getLugar().getNombre(), t1.getNombre());
        assertTrue(lugarDaoNeo4j.existeRuta(personaje.getLugar().getNombre(),t1.getNombre()));
    }

    @Test
    public void personajeConMonedasPuedeMoversePorLaRutaMasCorta() {
        Tienda t = new Tienda("tie99");
        Tienda t1 = new Tienda("tie100");
        String tipoCamino = "terrestre";
        serviceLugar.crearUbicacion(t);
        serviceLugar.crearUbicacion(t1);
        serviceLugar.conectar(t.getNombre(), t1.getNombre(), tipoCamino);
        Personaje pjn = new Personaje(humano, "ron", picaro);
        pjn.setBilletera(500);
        pjn.setLugar(t);
        run(() -> { personajeDAOH.guardar(pjn); return null; });
        serviceLugar.moverMasCorto(pjn.getId(), t1.getId());
        Personaje pjr = run(() -> { return personajeDAOH.recuperar(pjn.getId()); });
        assertEquals(pjr.getLugar().getNombre(), t1.getNombre());
        assertTrue(pjr.getBilletera() == 499);
    }



    @Test
    public void testListarItems() {
        List<Item> listaItemsm = new ArrayList<Item>();
        Personaje zaneB = this.serviceLugar.recuperarPersonaje(zane.getId());

        listaItemsm = this.serviceLugar.listarItems(zane.getId());

        assertEquals(listaItemsm.size(),2);
    }

    @Test
    public void testComprarItem(){

         this.serviceLugar.comprarItem(zane.getId(),casco.getId());
         this.serviceLugar.comprarItem(zane.getId(),espada.getId());
         Personaje zaneB = this.serviceLugar.recuperarPersonaje(zane.getId());

         assertEquals(zaneB.getMochila().size(),3);
         assertEquals(zaneB.getBilletera(),974,0);

    }

    @Test
    public void testVenderItem(){

        this.serviceLugar.venderItem(zane.getId(),casco.getId());
        Personaje zaneB = this.serviceLugar.recuperarPersonaje(zane.getId());

        assertEquals(zaneB.getMochila().size(),0);
        assertEquals(zaneB.getBilletera(),1050,0);

    }
}


