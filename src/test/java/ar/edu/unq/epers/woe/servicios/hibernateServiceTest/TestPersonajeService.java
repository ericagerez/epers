package ar.edu.unq.epers.woe.servicios.hibernateServiceTest;

import static org.junit.Assert.*;

import ar.edu.unq.epers.woe.backend.dao.hibernate.*;
import ar.edu.unq.epers.woe.backend.dao.mongo.EventoDAO;
import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.combate.ResultadoCombate;
import ar.edu.unq.epers.woe.backend.model.inventario.Inventario;
import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.TipoItem;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import ar.edu.unq.epers.woe.backend.model.lugar.Gimnasio;
import ar.edu.unq.epers.woe.backend.model.misiones.Mision;
import ar.edu.unq.epers.woe.backend.model.misiones.RecompensaItem;
import ar.edu.unq.epers.woe.backend.model.misiones.RequisitoMisionObtenerItem;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.service.hibernate.personaje.PersonajeImplService;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.SessionFactoryProvider;
import ar.edu.unq.epers.woe.backend.service.mongo.FeedServiceImpl;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

public class TestPersonajeService {

    private PersonajeImplService personajeImplService;

    private Personaje zaneBradford,hope;

    private Item espada,espada2,itemManoDerechaRival,itemManoIzquierdaRival;
    private ArrayList<Atributo> atributosItemManoDer,atributosItemManoIzq,atributosPersonaje, atributosItemManoDerRival, atributosItemManoIzqRival,atributosRival ;
    private Atributo dannoManoDer, dannoManoIzq, armadura,fuerza,destreza,vida,dannoArco,dannoEspada,armaduraRival,fuerzaRival,destrezaRival,vidaRival ;
    private Inventario inventario, inventarioRival;
    private Raza vampiro, elf;
    private Clase claseMago;
    private Gimnasio gimnasio;
    private ResultadoCombate resultadoCombate;
    private FeedServiceImpl feedService;

    @Before
    public void prepare(){
        this.personajeImplService = new PersonajeImplService(new PersonajeDAO(), new ItemDAO(), new ClaseDataDAO(), new GimnasioDao(), new ResultadoCombateDao(), new EventoDAO());
        feedService = new FeedServiceImpl();
        this.personajeImplService.setFeedService(feedService);
        resultadoCombate = new ResultadoCombate();

        atributosItemManoDer = new ArrayList<>();
        atributosItemManoIzq = new ArrayList<>();
        atributosPersonaje = new ArrayList<>();

        dannoManoDer = new Atributo(TipoAtributo.DANNO,50);
        dannoManoIzq = new Atributo(TipoAtributo.DANNO,50);
        armadura = new Atributo(TipoAtributo.ARMADURA,100);
        fuerza= new Atributo(TipoAtributo.FUERZA,25);
        destreza= new Atributo(TipoAtributo.DESTREZA,10);
        vida = new Atributo(TipoAtributo.VIDA,1000);


        espada = new Item("ItemManoDerecha", Ubicacion.MANO_DER, TipoItem.ESPADA, 10, 15);
        personajeImplService.guardarItem(espada);

        espada2 = new Item("ItemManoIzquierda", Ubicacion.MANO_IZQ, TipoItem.ESPADA, 10, 15);
        personajeImplService.guardarItem(espada2);

        atributosItemManoDer.add(dannoManoDer);
        espada.setAtributos(atributosItemManoDer);
        atributosItemManoIzq.add(dannoManoIzq);
        espada2.setAtributos(atributosItemManoIzq);

        atributosPersonaje.add(fuerza);
        atributosPersonaje.add(destreza);
        atributosPersonaje.add(vida);
        atributosPersonaje.add(armadura);

        inventario = new Inventario();
        inventario.addItem(espada);
        inventario.addItem(espada2);

        vampiro = new Raza("Vampiro");
        claseMago = new Clase("Mago");
        zaneBradford= new Personaje(vampiro,"zane",claseMago);
        zaneBradford.setInventario(inventario);

        inventario.equipar(espada,zaneBradford);


        zaneBradford.setAtributos(atributosPersonaje);

        ///Rival Personaje
        atributosItemManoDerRival = new ArrayList<Atributo>();
        atributosItemManoIzqRival = new ArrayList<Atributo>();
        atributosRival = new ArrayList<Atributo>();

        dannoArco = new Atributo(TipoAtributo.DANNO,100);
        dannoEspada = new Atributo(TipoAtributo.DANNO,50);
        armaduraRival = new Atributo(TipoAtributo.ARMADURA,200);
        fuerzaRival= new Atributo(TipoAtributo.FUERZA,10);
        destrezaRival= new Atributo(TipoAtributo.DESTREZA,10);
        vidaRival = new Atributo(TipoAtributo.VIDA,100);

        itemManoDerechaRival = new Item("ItemManoDerecha", Ubicacion.MANO_DER, TipoItem.ARCO, 10, 15);
        itemManoIzquierdaRival = new Item("ItemManoIzquierda", Ubicacion.MANO_IZQ, TipoItem.ESPADA, 10, 15);

        itemManoDerechaRival.addAtributo(dannoArco);
        itemManoIzquierdaRival.addAtributo(dannoEspada);

        atributosRival.add(fuerzaRival);
        atributosRival.add(destrezaRival);
        atributosRival.add(vidaRival);
        atributosRival.add(armaduraRival);

        inventarioRival= new Inventario();
        inventarioRival.addItem(itemManoDerechaRival);
        inventarioRival.addItem(itemManoIzquierdaRival);

        elf = new Raza("elf");
        hope= new Personaje(elf,"hope",claseMago);
        gimnasio = new Gimnasio("gimnasio",hope);
        zaneBradford.setLugar(gimnasio);
        ((Personaje) hope).setInventario(inventarioRival);
        inventarioRival.equipar(itemManoDerechaRival, (Personaje) hope);
        inventarioRival.equipar(itemManoIzquierdaRival, (Personaje) hope);
        ((Personaje) hope).setAtributos(atributosRival);
        hope.setLugar(gimnasio);


        personajeImplService.guardarGimnasio(gimnasio);

        personajeImplService.guardarPersonaje(zaneBradford);
        personajeImplService.guardarPersonaje(hope);

    }

    @After
    public void cleanup() {
        SessionFactoryProvider.destroy();
    }


    @Test
    public void testEquipar(){

        Personaje zane =this.personajeImplService.recuperarPersonaje(zaneBradford.getId());
        this.personajeImplService.equipar(zane.getId(),espada.getId());
        assertEquals("zane", zane.getNombre());

        assertTrue(zaneBradford.getInventario().getAllItems().contains(espada));
    }

    @Test
    public void testCombatir(){

        ResultadoCombate resultado = this.personajeImplService.combatir(zaneBradford.getId(),hope.getId());
       // this.personajeImplService.guardarResultadoCombate(resultado);


        Personaje hopi = this.personajeImplService.recuperarPersonaje(hope.getId());
        assertEquals("hope", hopi.getNombre());
        assertEquals(hopi.getLugar().getNombre(),"gimnasio");

        Personaje zane = this.personajeImplService.recuperarPersonaje(zaneBradford.getId());
        assertEquals("zane", zane.getNombre());

        ResultadoCombate res = this.personajeImplService.recuperarResultado(resultado.getId());
        assertEquals(resultado.getGanador().getId(), zane.getId());


    }
}
