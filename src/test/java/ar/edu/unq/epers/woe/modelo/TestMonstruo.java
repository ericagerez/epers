package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.monstruo.Monstruo;
import ar.edu.unq.epers.woe.backend.model.monstruo.TipoMonstruo;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;



public class TestMonstruo {
    private Monstruo monstruoLobo;

    @Before
    public void setUp() {
        monstruoLobo = new Monstruo(5, 50, 20, TipoMonstruo.LOBO);}


    @Test
    public void creoUnNuevoMonstruo(){
        assertTrue(5 == monstruoLobo.getVida());
        assertTrue(50 ==monstruoLobo.getDanno());
        assertEquals(TipoMonstruo.LOBO,monstruoLobo.getTipo());
    }
}
