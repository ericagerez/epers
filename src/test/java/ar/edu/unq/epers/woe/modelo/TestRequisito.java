package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.model.requerimiento.*;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class TestRequisito {

    private Personaje zaneBradford;
    private Raza vampiro;
    private Clase claseMago;
    private RequisitoDestreza requisitoDestreza;
    private RequisitoFuerza requisitoFuerza;
    private RequisitoNivel requisitoNivel;
    private Requerimiento requerimiento;
    private Atributo atributoFuerza;
    private Atributo atributoDestreza;


    @Before
    public void setUp() {

        requisitoFuerza = new RequisitoFuerza(10);
        requisitoNivel = new RequisitoNivel(1);
        requisitoDestreza = new RequisitoDestreza(10);

        ArrayList<Requisito> listR = new ArrayList<Requisito>();
        listR.add(requisitoDestreza);
        listR.add(requisitoFuerza);
        listR.add(requisitoNivel);

        requerimiento = new Requerimiento(listR);

        atributoDestreza = new Atributo(TipoAtributo.DESTREZA, 15);
        atributoFuerza   = new Atributo(TipoAtributo.FUERZA, 20);

        ArrayList<Atributo> listA = new ArrayList<Atributo>();
        listA.add(atributoDestreza);
        listA.add(atributoFuerza);

        vampiro = new Raza("vampiro");
        claseMago = new Clase("Mago");
        zaneBradford= new Personaje(vampiro,"zaneBradford1",claseMago);
        zaneBradford.setAtributos(listA);

    }

    @Test
    public void seVerificaLaCantidadDeRequisitosDeUnRequerimiento(){
        assertEquals(3, requerimiento.getRequisitos().size());
    }

    @Test
    public void seVerificaQueSeSatisfagaUnRequisitoNivel(){
        assertTrue(requisitoNivel.satisface(zaneBradford));
    }

    @Test
    public void seVerificaQueSeSatisfagaUnRequisitoDestreza(){
        assertTrue(requisitoDestreza.satisface(zaneBradford));
    }

    @Test
    public void seVerificaQueSeSatisfagaUnRequisitoFuerza(){
        assertTrue(requisitoFuerza.satisface(zaneBradford));
    }

}
