package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.TipoItem;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import ar.edu.unq.epers.woe.backend.model.lugar.Tienda;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;


public class TestTienda {
    private Tienda tienda;
    private Personaje personaje;
    private Item item1;
    private Item item2;
    private Raza razavampiro;
    private Clase clase;
    @Before
    public void setUp(){
        tienda = new Tienda("tiendaDeItemsUbicacionCabeza");

        razavampiro = mock(Raza.class);
        clase = mock(Clase.class);
        personaje = new Personaje(razavampiro,"ron",clase);

        item1 = new Item("CascoDeGuerra",Ubicacion.CABEZA,TipoItem.CASCO,100,50);
        item2 = new Item("Espada",Ubicacion.MANO_DER,TipoItem.ESPADA,200,150);

    }

    @Test
    public void tengoUnaTiendaNuevaSinItemsParaVender(){
        assertTrue(tienda.estaVacia());
    }

    @Test
    public void tengoUnaTiendaVaciaLeAgregoItemsParaVender(){
        tienda.agregarItem(item1);
        assertTrue(tienda.getItemsAvender().contains(item1));
    }

    @Test
    public void UnaTiendaConUnItemVieneUnPersonajeYQuiereComprarUnItemDeUbicacionCAbeza(){
        tienda.agregarItem(item1);
        personaje.setBilletera(100);
        tienda.venderItemAlPersonaje(personaje,Ubicacion.CABEZA);

        assertTrue(tienda.estaVacia());
        assertEquals(50,personaje.getBilletera());
        assertTrue(personaje.contieneElItemLaMochila(item1));
    }

    @Test
    public void unPersonajeSeHacercaALaTiendaParaVenderUnItemQuePosee(){

        personaje.guarItemEnMochila(item1);
        assertTrue(personaje.contieneElItemLaMochila(item1));

        tienda.comprarItemAlPersonaje(personaje,item1);
        assertFalse(tienda.estaVacia());
        assertEquals(item1,tienda.buscarItemConubicacion(Ubicacion.CABEZA));
        assertFalse(personaje.contieneElItemLaMochila(item1));
        assertEquals(100,personaje.getBilletera());

    }


}
