package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.lugar.Taberna;
import ar.edu.unq.epers.woe.backend.model.misiones.Mision;
import ar.edu.unq.epers.woe.backend.model.misiones.Recompensa;
import ar.edu.unq.epers.woe.backend.model.misiones.RecompensaItem;
import ar.edu.unq.epers.woe.backend.model.misiones.RequisitoMisionObtenerItem;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import static org.mockito.Mockito.mock;
import static org.junit.Assert.*;

import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class TestTaberna {
    public Taberna taberna;
    public Personaje personaje;
    public Mision mision;
    public Mision mision2;
    public Mision mision3;


    @Before
    public void setUp(){
        taberna = new Taberna("taberna los vengadores");
        Item item = mock(Item.class);
        Item item2 = mock(Item.class);
        List<Item> listaItem = new ArrayList<>();
        listaItem.add(item);
        listaItem.add(item);

        personaje = new Personaje(new Raza("personaje"),"personaje",new Clase("mago"));
        personaje.setLugar(taberna);

        mision= new Mision("mision",new RequisitoMisionObtenerItem(item), new RecompensaItem(listaItem));
        mision2 = new Mision("mision2",new RequisitoMisionObtenerItem(),new RecompensaItem(listaItem));
        mision3 = new Mision("mision3",new RequisitoMisionObtenerItem(),new RecompensaItem(listaItem));
        mision.getMisionesHijas().add(mision3);



    }

    @Test
    public void unaTabernaRecienCreadaNoTieneMisiones(){
        assertEquals(0,taberna.getMisiones().size());
    }

    @Test
    public void unaTabernaLeAgregamosUnaMision(){
        taberna.addMision(mision);

        assertTrue(taberna.getMisiones().contains(mision));
    }

    @Test
    public void unaTabernaCOnUnaMisionUnPersonajeQuiereAceptarla(){
        taberna.addMision(mision);
        taberna.aceptarMision(personaje,mision);


        assertTrue(personaje.getMisionesAceptadas().contains(mision));
    }

    @Test
    public void unaTabaernaYunPersonajeSInMisionesAcpetadasListaMisiones(){
        taberna.addMision(mision);

        taberna.addMision(mision2);

        List<Mision> resultado = new ArrayList<>();
        resultado.add(mision);
        resultado.add(mision2);
        assertEquals(resultado,taberna.listarMisiones(personaje));
    }

    @Test
    public void unaTabernaYIUnPersonaeQUeAceptoUnaDeLasMisionesLeHacemosListarMision(){
        taberna.addMision(mision);

        taberna.addMision(mision2);
        taberna.aceptarMision(personaje,mision);

        assertEquals(1,personaje.getMisionesAceptadas().size() );
        assertEquals(2,taberna.getMisiones().size() );

        List<Mision> resultado = new ArrayList<>();
        resultado.add(mision3);
        resultado.add(mision2);
        assertEquals(resultado,taberna.listarMisiones(personaje));

    }
}
