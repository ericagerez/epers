package ar.edu.unq.epers.woe.servicios.mongoTest;

import ar.edu.unq.epers.woe.backend.dao.Neo4j.LugarDaoNeo4j.LugarDaoNeo4j;
import ar.edu.unq.epers.woe.backend.dao.mongo.EventoDAO;
import ar.edu.unq.epers.woe.backend.model.eventos.*;
import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.lugar.Tienda;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.service.mongo.FeedServiceImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestFeedServiceMongo {
    FeedServiceImpl feedServ;
    EventoDAO eventoDao;
    LugarDaoNeo4j lugarDao;

    @Before
    public void setUp(){
        feedServ = new FeedServiceImpl();
        eventoDao = new EventoDAO(); eventoDao.eliminarDatos();
        lugarDao = new LugarDaoNeo4j();
        lugarDao.tearDown(); //clean Neo4J
    }

    @Test
    public void seRecuperanSeisEventosRelacionadosAUnPersonaje() {

        Personaje p = new Personaje(null, "testP1", null);

        Evento e = new Ganador(p.getNombre(), "testL", null, "tstClase0", "tstClase1", "tstRaza0", "tstRaza1");
        Evento e2 = new MisionAceptada(p.getNombre(), "testL", "tstMision1");
        Evento e3 = new MisionCompletada(p.getNombre(), "testL", "tstMision2");
        Evento e4 = new Arribo(p.getNombre(), "tLugarOrg", "tClaseLOrg", "tLugarDest", "tClaseLDest");
        Evento e5 = new ComprarItem(p.getNombre(), "testL", "testItem", 0);
        Evento e6 = new VenderItem(p.getNombre(), "testL", "testItem", 0);

        Evento eNonRelated = new ComprarItem( "testP2", "testL", "testItem", 0);

        eventoDao.save(e); eventoDao.save(e2); eventoDao.save(e3);
        eventoDao.save(e4); eventoDao.save(e5); eventoDao.save(e6);
        eventoDao.save(eNonRelated);

        List<Evento> eventos = feedServ.feedPersonaje( p.getNombre() );
        assertEquals( 6, eventos.size() );
        assertEquals( e6.getFecha(), eventos.get(0) .getFecha() ); //mas reciente
        assertEquals( e.getFecha(), eventos.get(5) .getFecha() ); //mas antiguo
    }

    @Test
    public void deUnPersonajeSinNingunEventoSeObtieneUnaListaVacia() {
        Personaje p = new Personaje(null, "testP1", null);

        Evento eNonRelated = new ComprarItem( "tstPjX", "testLugar", "testItem", 0);
        eventoDao.save(eNonRelated);

        assertTrue( feedServ.feedPersonaje( p.getNombre() ).isEmpty() );
    }

    @Test
    public void seRecuperanSeisEventosRelacionadosAUnLugarYASusConexiones() {

        Lugar lugar = new Tienda("testLugar");
        Lugar lugar2 = new Tienda("testLugar2");
        Lugar lugar3 = new Tienda("testLugar3");

        lugarDao.create(lugar); lugarDao.create(lugar2); lugarDao.create(lugar3);
        String tipoCamino = "terrestre";
        lugarDao.crearRuta(lugar.getNombre(), lugar2.getNombre(), tipoCamino );
        lugarDao.crearRuta(lugar.getNombre(), lugar3.getNombre(), tipoCamino );

        //eventos con lugar
        Evento e = new Ganador("tstPj", lugar.getNombre(), null, "tstClase0", "tstClase1", "tstRaza0", "tstRaza1");
        //eventos con lugar2
        Evento e2 = new MisionAceptada("tstPj", lugar2.getNombre(), "tstMision1");
        Evento e3 = new MisionCompletada( "tstPj", lugar2.getNombre(), "tstMision1");
        Evento e4 = new Arribo( "tstPj", lugar2.getNombre(), "tClaseLOrg", "tLugarDest", "tClaseLDest");
        //eventos con lugar3
        Evento e5 = new ComprarItem( "tstPj", lugar3.getNombre(), "testItem", 0);
        Evento e6 = new VenderItem( "tstPj", lugar3.getNombre(), "testItem", 0);

        Evento eNonRelated = new ComprarItem( "tstPj", "testLugarX", "testItem", 0);

        eventoDao.save(e); eventoDao.save(e2); eventoDao.save(e3);
        eventoDao.save(e4); eventoDao.save(e5); eventoDao.save(e6);
        eventoDao.save(eNonRelated);

        List<Evento> eventos = feedServ.feedLugar( lugar.getNombre() );
        assertEquals( 6, eventos.size() );
        assertEquals( e6.getFecha(), eventos.get(0).getFecha() );//mas reciente
        assertEquals( e5.getFecha(), eventos.get(1).getFecha() );//segundo mas reciente
    }

    @Test
    public void deUnLugarSinNingunEventoSeObtieneUnaListaVacia() {
        Lugar lugar = new Tienda("testLugar");
        lugarDao.create(lugar);

        Evento eNonRelated = new ComprarItem( "tstPj", "testLugarX", "testItem", 0);
        eventoDao.save(eNonRelated);

        assertTrue( feedServ.feedLugar(lugar.getNombre()) .isEmpty() );
    }

}
