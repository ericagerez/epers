package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.TipoItem;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import ar.edu.unq.epers.woe.backend.model.lugar.Taberna;
import ar.edu.unq.epers.woe.backend.model.misiones.Mision;
import ar.edu.unq.epers.woe.backend.model.misiones.Recompensa;
import ar.edu.unq.epers.woe.backend.model.misiones.RequisitoMision;
import ar.edu.unq.epers.woe.backend.model.personaje.ExceptionAtributo;
import ar.edu.unq.epers.woe.backend.model.personaje.ExceptionMochila;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import org.junit.Test;
import org.junit.Before;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestPersonaje {

    private Personaje zaneBradford;
    private Raza vampiro;
    private Set<Clase> clasesDeRaza;
    private Clase claseMago;


    @Before
    public void setUp() {

        vampiro = new Raza("vampiro");
        claseMago = mock(Clase.class);
        clasesDeRaza = new HashSet<>();
        clasesDeRaza.add(claseMago);// ver de mokear la clase


        zaneBradford= new Personaje(vampiro,"zaneBradford1",claseMago);

    }

    @Test
    public void nuevoPersonajeRecienInicializoConNivelInicial1(){
        assertEquals("zaneBradford1",zaneBradford.getNombre());
        assertEquals(vampiro, zaneBradford.getRaza());
        assertEquals(claseMago, zaneBradford.getClase());
        assertEquals(1,zaneBradford.getNivel());
        assertEquals(100,zaneBradford.getCantPuntosDeExperiencia());
        assertEquals(0,zaneBradford.getBilletera());
        assertTrue(100 == zaneBradford.getEnergia());


    }
// Testeando diferentes Niveles
    @Test
    public void miPersonajeGana50PuntosDeExperienciaYNoPuedePasarDeNivel(){
        zaneBradford.ganaPuntosDeExperiencia(50);
        assertEquals(150,zaneBradford.getCantPuntosDeExperiencia());
        assertEquals(1,zaneBradford.getNivel());
    }

    @Test
    public void miPersonajeEstaEnElNivelGana100PuntosMasDeExperienciaYPasaAlNivel2(){
        zaneBradford.ganaPuntosDeExperiencia(100);
        assertEquals(200,zaneBradford.getCantPuntosDeExperiencia());
        assertEquals(2,zaneBradford.getNivel());

    }
    @Test
    public void miPersonajeEstaEnElNivel2yGana50PuntosDeEXPYNOPuedePasarAlNivelTres(){
        zaneBradford.ganaPuntosDeExperiencia(100);
        assertEquals(2,zaneBradford.getNivel());
        zaneBradford.ganaPuntosDeExperiencia(50);
        assertFalse(zaneBradford.puedoSubirDeNivel());
    }

    @Test
    public void miPersonajeDeNivel1Gana900PuntosDeExperienciaHaciendoPasarAlNivel10(){
        assertEquals(1,zaneBradford.getNivel());
        zaneBradford.ganaPuntosDeExperiencia(900);
        assertEquals(1000,zaneBradford.getCantPuntosDeExperiencia());
        assertEquals(10,zaneBradford.getNivel());
    }

    @Test
    public void miPersonajeDeNivel10Gana2200YPasaAlNivel12(){
        zaneBradford.ganaPuntosDeExperiencia(900);
        assertEquals(1000,zaneBradford.getCantPuntosDeExperiencia());
        zaneBradford.ganaPuntosDeExperiencia(2200);
        assertEquals(3200,zaneBradford.getCantPuntosDeExperiencia());
        assertEquals(12,zaneBradford.getNivel());
    }




    //Mochila
    @Test
    public void miPersonajeNoContieneNingunItemEnSuMochila(){
        assertTrue(zaneBradford.getMochila().isEmpty());
    }

    @Test
    public void miPersonajeObtieneSuPrimerItem(){
        Item item1 = new Item("unItem",Ubicacion.CABEZA,TipoItem.CASCO,100,50);

        zaneBradford.guarItemEnMochila(item1);
       // assertTrue(zaneBradford.cumploRequisitos(item1.getRequisitos()));
        assertTrue( zaneBradford.contieneElItemLaMochila(item1));

    }

   @Test
    public void miPersonajeObtieneDosItems(){
        Item item1 = mock(Item.class);
        Item item2 = mock(Item.class);
        zaneBradford.guarItemEnMochila(item1);
        zaneBradford.guarItemEnMochila(item2);

        assertEquals(2,zaneBradford.getMochila().size());
        assertTrue( zaneBradford.contieneElItemLaMochila(item1));
        assertTrue( zaneBradford.contieneElItemLaMochila(item2));

    }


    // Atributos

    @Test
    public void miPersonajeNoNitieneNingunAtributo(){
        assertTrue(zaneBradford.getAtributos().isEmpty());
    }

    @Test
    public void miPersonajeGuardaUnAtributoNuevo(){
        Atributo atributo= mock(Atributo.class);
        zaneBradford.guardarAtributo(atributo);
        assertTrue(zaneBradford.contieneEsteAtributo(atributo));
    }



   /// TABERNA

    @Test
    public void miPersonajeEstaEnUnaTabernaYAceptaUnaMision(){
        zaneBradford.setLugar(new Taberna());
        RequisitoMision requisitoMision = mock(RequisitoMision.class);
        Recompensa recompensa = mock(Recompensa.class);
        Mision mision = new Mision("nombreM",requisitoMision,recompensa);

        zaneBradford.aceptarMision(mision);

        assertTrue(zaneBradford.getMisionesAceptadas().contains(mision));
    }


}

