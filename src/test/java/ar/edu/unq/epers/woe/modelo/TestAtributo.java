package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class TestAtributo {
    private Atributo atributo;
    private TipoAtributo fuerza;

    @Before
    public void setUp() {
        fuerza = TipoAtributo.FUERZA;
        atributo = new Atributo(fuerza,3);
    }

    @Test
    public void unAtributoConTipoFuerzaYValor3(){
        assertTrue( atributo.getValor() == 3.00);
        assertEquals(TipoAtributo.FUERZA,atributo.getTipo());
    }
}
