package ar.edu.unq.epers.woe.servicios.JDBCServiceTest;

import ar.edu.unq.epers.woe.backend.dao.JDBC.JDBCRazaDAOJDBC;
import ar.edu.unq.epers.woe.backend.service.data.JDBCDataService;
import ar.edu.unq.epers.woe.backend.service.data.JDBCDataServiceImplementation;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestJDBCDataService {
    private JDBCDataService JDBCDataService;
    private JDBCRazaDAOJDBC razaDAO;

    @Before
    public void setUp() {
        JDBCDataService = new JDBCDataServiceImplementation();
        razaDAO = new JDBCRazaDAOJDBC();

    }

    @After
    public void limpiarBaseDeDatos() {
       JDBCDataService.eliminarDatos();
    }

    @Test
    public void crearDatosInicialesDeUnaRaza(){
        JDBCDataService.crearSetDatosIniciales();
        assertTrue(razaDAO.recuperar(1).getId() == 1);
        assertEquals(razaDAO.recuperarTodos().size(),1);

    }
    @Test
    public void tenemosDatosInicialesEnLaBDLoEliminamos(){
       //creamos los datos iniciales
        JDBCDataService.crearSetDatosIniciales();
        assertEquals(razaDAO.recuperarTodos().size(),1);// hay datos
        //eliminamos los datos
        JDBCDataService.eliminarDatos();
        assertTrue(razaDAO.recuperarTodos().isEmpty());// la bd esta vacia
    }

}