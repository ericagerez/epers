package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.combate.Combate;
import ar.edu.unq.epers.woe.backend.model.combate.ResultadoCombate;
import ar.edu.unq.epers.woe.backend.model.inventario.Inventario;
import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.TipoItem;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import ar.edu.unq.epers.woe.backend.model.monstruo.Monstruo;
import ar.edu.unq.epers.woe.backend.model.monstruo.TipoMonstruo;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

public class TestCombate {

    private Combate combate;

    private Personaje personaje;
    private Rival rival,monstruo;
    private Raza vampiro;
    private Clase claseMago;
    private Clase claseGuerrero;
    private Item itemManoDerecha,itemManoIzquierda,itemManoDerechaRival,itemManoIzquierdaRival;
    private Inventario inventario,inventarioRival;
    private ArrayList<Atributo> atributosItemManoDer,atributosItemManoIzq,atributosPersonaje,atributosRival,atributosItemManoDerRival,atributosItemManoIzqRival;
    private Atributo danno, armadura,fuerza,destreza,vida,dannoArco,armaduraRival,destrezaRival,fuerzaRival,vidaRival,dannoEspada;
    private ResultadoCombate resultado;

    @Before
    public void setUp(){
        resultado = new ResultadoCombate();

        //////PERSONAJE
        atributosItemManoDer = new ArrayList<Atributo>();
        atributosItemManoIzq = new ArrayList<Atributo>();
        atributosPersonaje = new ArrayList<Atributo>();

        danno = new Atributo(TipoAtributo.DANNO,50);
        armadura = new Atributo(TipoAtributo.ARMADURA,100);
        fuerza= new Atributo(TipoAtributo.FUERZA,25);
        destreza= new Atributo(TipoAtributo.DESTREZA,10);
        vida = new Atributo(TipoAtributo.VIDA,1000);

        itemManoDerecha = new Item("ItemManoDerecha", Ubicacion.MANO_DER, TipoItem.ESPADA, 10, 15);
        itemManoIzquierda = new Item("ItemManoIzquierda", Ubicacion.MANO_IZQ, TipoItem.ESPADA, 10, 15);

        atributosItemManoDer.add(danno);
        itemManoDerecha.setAtributos(atributosItemManoDer);
        atributosItemManoIzq.add(danno);
        itemManoIzquierda.setAtributos(atributosItemManoIzq);

        atributosPersonaje.add(fuerza);
        atributosPersonaje.add(destreza);
        atributosPersonaje.add(vida);
        atributosPersonaje.add(armadura);

        inventario = new Inventario();

        personaje= new Personaje(vampiro,"ron",claseMago);
        personaje.setInventario(inventario);
        inventario.equipar(itemManoDerecha,personaje);
        inventario.equipar(itemManoIzquierda,personaje);
        personaje.setAtributos(atributosPersonaje);

        ////RIVAL PERSONAJE
        atributosItemManoDerRival = new ArrayList<Atributo>();
        atributosItemManoIzqRival = new ArrayList<Atributo>();
        atributosRival = new ArrayList<Atributo>();

        dannoArco = new Atributo(TipoAtributo.DANNO,100);
        dannoEspada = new Atributo(TipoAtributo.DANNO,50);
        armaduraRival = new Atributo(TipoAtributo.ARMADURA,200);
        fuerzaRival= new Atributo(TipoAtributo.FUERZA,10);
        destrezaRival= new Atributo(TipoAtributo.DESTREZA,10);
        vidaRival = new Atributo(TipoAtributo.VIDA,100);

        itemManoDerechaRival = new Item("ItemManoDerecha", Ubicacion.MANO_DER, TipoItem.ARCO, 10, 15);
        itemManoIzquierdaRival = new Item("ItemManoIzquierda", Ubicacion.MANO_IZQ, TipoItem.ESPADA, 10, 15);

        itemManoDerechaRival.addAtributo(dannoArco);
        itemManoIzquierdaRival.addAtributo(dannoEspada);

        atributosRival.add(fuerzaRival);
        atributosRival.add(destrezaRival);
        atributosRival.add(vidaRival);
        atributosRival.add(armaduraRival);

        inventarioRival= new Inventario();
        inventarioRival.addItem(itemManoDerechaRival);
        inventarioRival.addItem(itemManoIzquierdaRival);

        rival = new Personaje(vampiro,"zane",claseGuerrero);
        ((Personaje) rival).setInventario(inventarioRival);
        inventarioRival.equipar(itemManoDerechaRival, (Personaje) rival);
        inventarioRival.equipar(itemManoIzquierdaRival, (Personaje) rival);
        ((Personaje) rival).setAtributos(atributosRival);

        ////RIVAL MONSTRUO
        monstruo = new Monstruo(500,300,30, TipoMonstruo.LOBO);

    }
    @Test
    public void calculoElDannoQueGeneraMiPersonaje(){
        assertEquals(itemManoDerecha.getAtributo(TipoAtributo.DANNO),danno);
        assertEquals(personaje.getDannoPersonaje(),1687.5675);
    }

    @Test
    public void calculoElDannoQueGeneraMiRival(){
        assertTrue(inventarioRival.getSlotDe(Ubicacion.MANO_DER).tieneItem());
        assertTrue(inventarioRival.getSlotDe(Ubicacion.MANO_IZQ).tieneItem());
        assertEquals(itemManoDerechaRival.getAtributo(TipoAtributo.DANNO),dannoArco);
        assertEquals(itemManoDerechaRival.getAtributo(TipoAtributo.DANNO).getValor(),100.0);
        assertEquals( rival.getDannoPersonaje(),1175.1174999999998);
    }

    @Test
    public void calculoLaDefensaQueTieneMiPersonaje(){
        assertEquals(personaje.getDefensa(),268.75675);
    }

    @Test
    public void calculoLaDefensaQueTieneMiRivalPersonaje(){
        assertEquals(rival.getDefensa(),317.51175);
    }

    @Test
    public void creoUnNuevoCombateConUnPersonajeYUnMonstruo(){
        combate= new Combate(personaje,monstruo);
        assertEquals(combate.getPersonaje(),personaje);
        assertEquals(combate.getRival(),monstruo);
    }

    @Test
    public void creoUnNuevoCombateConUnPersonajeYUnMonstruoYVerificoQuienGana(){
        combate= new Combate(personaje,monstruo);

       ResultadoCombate resultado =combate.realizarCombate(personaje,monstruo);
        assertEquals(personaje.getDannoPersonaje(),1687.5675);
        assertEquals(monstruo.getDannoPersonaje(),300.0);
        assertEquals(resultado.getGanador(),personaje);
    }


    @Test
    public void creoUnNuevoCombateConDosPersonajes(){
        combate= new Combate(personaje,rival);
        assertEquals(combate.getPersonaje(),personaje);
        assertEquals(combate.getRival(),rival);
    }

    @Test
    public void creoUnNuevoCombateConDosPeronajesYVerificoQueNoHayGanador(){
        combate = new Combate(personaje,rival);
        assertTrue(resultado.noHayGanador(personaje,rival,personaje.getVida(),rival.getVida()));
    }


    @Test
    public void creoUnNuevoCombateConUnPersonajeYOtroPersonadeYVerificoQuienGana(){
        combate= new Combate(personaje,rival);

        ResultadoCombate resultado =combate.realizarCombate(personaje,rival);
        assertEquals(personaje.getDannoPersonaje(),1687.5675);
        assertEquals(rival.getDannoPersonaje(),1175.1174999999998);
        assertEquals(resultado.getGanador(),personaje);
    }



}
