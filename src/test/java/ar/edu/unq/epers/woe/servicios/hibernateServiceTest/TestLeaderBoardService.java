package ar.edu.unq.epers.woe.servicios.hibernateServiceTest;

import ar.edu.unq.epers.woe.backend.dao.hibernate.ClaseDAO;
import ar.edu.unq.epers.woe.backend.dao.hibernate.PersonajeDAO;
import ar.edu.unq.epers.woe.backend.dao.hibernate.RazaDAO;
import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.service.hibernate.leaderboard.LeaderboardImplService;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.SessionFactoryProvider;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;

public class TestLeaderBoardService {

    private LeaderboardImplService leaderBoardImplService;

    private Personaje noOne,hope, zane, lau,bane, luke,jack,harry,frodo,willy;
    private Raza humano,goliat,elf,vampiro,enano,elfoDeLaNoche,orco,huargen,horda,draenei,gnomo;
    private Clase mago,guerrero,druida,monje,brujo,caballeroDeLaMuerte,sacerdote,cazador,chaman,picaro,paladin;
    private Atributo fuerzaN,fuerzaH,fuerzaZ,fuerzaL,fuerzaB,fuerzaK,fuerzaJ,fuerzaY,fuerzaF,fuerzaW, dannoN, dannoH, dannoZ, dannoL, dannoB, dannoK, dannoJ, dannoY, dannoF, dannoW;
    private ArrayList<Atributo> atributosNoOne, atributosHope, atributosZane, atributosLau, atributosBane, atributosLuke, atributosJack, atributosHarry, atributosFrodo, atributosWilly;

    @Before
    public void prepare(){

        this.leaderBoardImplService= new LeaderboardImplService(new PersonajeDAO(),new RazaDAO(),new ClaseDAO());

        atributosNoOne= new ArrayList<Atributo>();
        atributosHope = new ArrayList<Atributo>();
        atributosZane = new ArrayList<Atributo>();
        atributosLau  = new ArrayList<Atributo>();
        atributosBane = new ArrayList<Atributo>();
        atributosLuke = new ArrayList<Atributo>();
        atributosJack = new ArrayList<Atributo>();
        atributosHarry= new ArrayList<Atributo>();
        atributosFrodo= new ArrayList<Atributo>();
        atributosWilly= new ArrayList<Atributo>();

        mago     = new Clase("mago");
        guerrero = new Clase("guerrero");
        druida   = new Clase("druida");
        monje    = new Clase("monje");
        paladin  = new Clase("paladin");
        cazador  = new Clase("cazador");
        picaro   = new Clase("picaro");
        sacerdote= new Clase("sacerdote");
        caballeroDeLaMuerte= new Clase("caballeroDeLaMuerte");
        brujo    = new Clase("brujo");
        chaman   = new Clase("chaman");

        humano   = new Raza("humano");
        goliat   = new Raza("goliat");
        elf      = new Raza("elf");
        enano    = new Raza("enano");
        elfoDeLaNoche  = new Raza("elfoDeLaNoche");
        vampiro  = new Raza("vampiro");
        gnomo  = new Raza("gnomo");
        draenei  = new Raza("draenei");
        huargen  = new Raza("huargen");
        horda  = new Raza("horda");
        orco  = new Raza("orco");

        fuerzaN  = new Atributo(TipoAtributo.FUERZA,25);
        dannoN  = new Atributo(TipoAtributo.DANNO,25);
        noOne    = new Personaje(vampiro,"noOne",mago);
        noOne.setCombatesGanados(5);
        atributosNoOne.add(fuerzaN);
        noOne.setAtributos(atributosNoOne);
        leaderBoardImplService.guardarPersonaje(noOne);


        fuerzaH  = new Atributo(TipoAtributo.FUERZA,30);
        dannoH  = new Atributo(TipoAtributo.DANNO,30);
        hope     = new Personaje(elf,"hope",chaman);
        hope.setCombatesGanados(20);
        hope.setAtributos(atributosHope);
        atributosHope.add(fuerzaH);
        atributosHope.add(dannoH);
        leaderBoardImplService.guardarPersonaje(hope);


        fuerzaZ  = new Atributo(TipoAtributo.FUERZA, 12);
        dannoZ  = new Atributo(TipoAtributo.DANNO,12);
        zane     = new Personaje(vampiro,"zane",caballeroDeLaMuerte);
        zane.setCombatesGanados(6);
        zane.setAtributos(atributosZane);
        atributosZane.add(fuerzaZ);
        atributosZane.add(dannoZ);
        leaderBoardImplService.guardarPersonaje(zane);

        fuerzaL  = new Atributo(TipoAtributo.FUERZA, 5);
        dannoL  = new Atributo(TipoAtributo.DANNO,5);
        lau      = new Personaje(humano,"lau",guerrero);
        lau.setCombatesGanados(11);
        lau.setAtributos(atributosLau);
        atributosLau.add(fuerzaL);
        atributosLau.add(dannoL);
        leaderBoardImplService.guardarPersonaje(lau);

        fuerzaB  = new Atributo(TipoAtributo.FUERZA, 60);
        dannoB  = new Atributo(TipoAtributo.DANNO,60);
        bane     = new Personaje(goliat,"bane",monje);
        bane.setCombatesGanados(2);
        bane.setAtributos(atributosBane);
        atributosBane.add(fuerzaB);
        atributosBane.add(dannoB);
        leaderBoardImplService.guardarPersonaje(bane);

        fuerzaK  = new Atributo(TipoAtributo.FUERZA, 90);
        dannoK  = new Atributo(TipoAtributo.DANNO,90);
        luke     = new Personaje(elf,"luke",sacerdote);
        luke.setCombatesGanados(1);
        luke.setAtributos(atributosLuke);
        atributosLuke.add(fuerzaK);
        atributosLuke.add(dannoK);
        leaderBoardImplService.guardarPersonaje(luke);


        fuerzaJ  = new Atributo(TipoAtributo.FUERZA, 40);
        dannoJ  = new Atributo(TipoAtributo.DANNO,40);
        jack     = new Personaje(vampiro,"jack",druida);
        jack.setCombatesGanados(7);
        jack.setAtributos(atributosJack);
        atributosJack.add(fuerzaJ);
        atributosJack.add(dannoJ);
        leaderBoardImplService.guardarPersonaje(jack);

        fuerzaY  = new Atributo(TipoAtributo.FUERZA, 2);
        dannoY  = new Atributo(TipoAtributo.DANNO,2);
        harry    = new Personaje(vampiro,"harry",picaro);
        harry.setCombatesGanados(3);
        harry.setAtributos(atributosHarry);
        atributosHarry.add(fuerzaY);
        atributosHarry.add(dannoY);
        leaderBoardImplService.guardarPersonaje(harry);

        fuerzaF  = new Atributo(TipoAtributo.FUERZA, 5);
        dannoF  = new Atributo(TipoAtributo.DANNO,5);
        frodo    = new Personaje(humano,"frodo",paladin);
        frodo.setCombatesGanados(9);
        frodo.setAtributos(atributosFrodo);
        atributosFrodo.add(fuerzaF);
        atributosFrodo.add(dannoF);
        leaderBoardImplService.guardarPersonaje(frodo);

        fuerzaW  = new Atributo(TipoAtributo.FUERZA, 15);
        dannoW  = new Atributo(TipoAtributo.DANNO,15);
        willy    = new Personaje(elf,"willy",brujo);
        willy.setCombatesGanados(0);
        willy.setAtributos(atributosWilly);
        atributosWilly.add(fuerzaW);
        atributosWilly.add(dannoW);
        leaderBoardImplService.guardarPersonaje(willy);


    }

    @After
    public void cleanup() {
        SessionFactoryProvider.destroy();
    }

    @Test
    public void testCampeones(){
        List<Personaje> campeones = new ArrayList<Personaje>();
           campeones= this.leaderBoardImplService.campeones();

        Assert.assertEquals(leaderBoardImplService.campeones().size(), 10);

    }

    @Test
    public void testRazaLider(){

       Raza razaLider= this.leaderBoardImplService.razaLider();

        Assert.assertEquals(razaLider.getNombre(),"vampiro");
    }

    @Test
    public void testClaseLider(){

        Clase claseLider= this.leaderBoardImplService.claseLider();

        Assert.assertEquals(claseLider.getNombre(),"chaman");
    }

    @Test
    public void testRazasPopulares(){
        List<Raza> razasPopulares = new ArrayList<Raza>();
        razasPopulares= this.leaderBoardImplService.razasPopulares();

        Assert.assertEquals(razasPopulares.size(),10);
    }

    @Test
    public void testClasesPopulares(){
        List<Clase> clasesPopulares = new ArrayList<Clase>();
        clasesPopulares= this.leaderBoardImplService.clasesPopulares();

        Assert.assertEquals(clasesPopulares.size(),10);
    }

    @Test
    public void testMasFuerte(){
       Personaje personajeMasFuerte= this.leaderBoardImplService.masFuerte();
        Assert.assertEquals(personajeMasFuerte.getNombre(),"luke");
    }


}
