package ar.edu.unq.epers.woe.modelo;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.TipoItem;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.requerimiento.Requisito;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;

public class TestItem {
    Item unItem;
    @Before
    public void setUp(){
        unItem = new Item("item1",Ubicacion.MANO_DER,TipoItem.ARCO,100,50);

    }

    @Test
    public void unItemRecienCreado(){
        assertEquals("item1",unItem.getNombre());
        assertEquals(Ubicacion.MANO_DER,unItem.getUbicacion());
        assertEquals(TipoItem.ARCO,unItem.getTipo());
        assertTrue( unItem.getCostoDeCompra() == 100);
        assertTrue(unItem.getCostoDeVenta() == 50);
    }

   /* @Test
    public void unItemLeAgregamosUnaClasePermitidaLaCualPodraUtiizadla(){
        assertTrue(unItem.getClasesPermitidas().isEmpty());

        unItem.addClase(Clase.MAGO);
        assertTrue(unItem.tieneLaClase(Clase.MAGO));
    }*/

    @Test
    public void unItemLeAgregamosUnRequerimiento(){
        Requisito requisito = mock(Requisito.class);
        assertTrue(unItem.getRequisitos().isEmpty());
        unItem.addRequerimiento(requisito);
        assertTrue(unItem.tieneElRequerimiento(requisito));
    }

    @Test
    public void unItemLeAgregamosUnAtributo(){
        Atributo atributo = mock(Atributo.class);
        assertTrue(unItem.getAtributos().isEmpty());

        unItem.addAtributo(atributo);
        assertTrue(unItem.tieneElAtributo(atributo));
    }


}
