package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;

public class ItemDAO extends AbstractaDAO<Item> {
    public ItemDAO(){super(Item.class);}


    @Override
    Class<Item> getClase() {
        return Item.class;
    }
}
