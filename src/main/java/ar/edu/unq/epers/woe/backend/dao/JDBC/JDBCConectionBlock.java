package ar.edu.unq.epers.woe.backend.dao.JDBC;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class JDBCConectionBlock {

    /**
     * Ejecuta un bloque de codigo contra una conexion.
     */
    static public <T> T executeWithConnection(ConnectionBlock<T> bloque) {
        Connection connection = JDBCConectionBlock.openConnection();
        try {
            return bloque.executeWith(connection);
        } catch (SQLException e) {
            throw new RuntimeException("Error no esperado", e);
        } finally {
            JDBCConectionBlock.closeConnection(connection);
        }

    }

    /**
     * Establece una conexion a la url especificada
     *
     * @return la conexion establecida
     */
    static private Connection openConnection() {
        try {
            return DriverManager.getConnection("jdbc:mysql://localhost:3306/epers_namelessteam_jdbc?user=root&password=enlasnubes17&serverTimezone=UTC");
        } catch (SQLException e) {
            throw new RuntimeException("No se puede establecer una conexion", e);
        }
    }

    /**
     * Cierra una conexion con la base de datos (libera los recursos utilizados por la misma)
     *
     * @param connection - la conexion a cerrar.
     */
    static private void closeConnection(Connection connection) {
        try {
            connection.close();
        } catch (SQLException e) {
            throw new RuntimeException("Error al cerrar la conexion", e);
        }
    }
}
