package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.misiones.Mision;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class PersonajeDAO extends AbstractaDAO<Personaje> {

    public PersonajeDAO() {
        super(Personaje.class);
    }

    public List<Personaje> getCampeones() {

        Session session = TransactionRunner.getCurrentSession();

        String hql = "from Personaje p "
                + "order by p.combatesGanados desc";

        Query<Personaje> query = session.createQuery(hql, Personaje.class);
        query.setMaxResults(10);

        return query.getResultList();
    }

    public Personaje getMasFuerte(){
    /** Retorna el personaje que produce más danno **/
        Session session = TransactionRunner.getCurrentSession();

        String hql ="select p "
                +"from Personaje p "
                +"join  p.atributos a "
                +"where a.tipo = 'DANNO' "
                + "order by a.valor desc";

        Query<Personaje> query = session.createQuery(hql, Personaje.class);
        query.setMaxResults(1);

        return query.getSingleResult();
    }

    @Override
    Class<Personaje> getClase() {
        return Personaje.class;
    }


}
