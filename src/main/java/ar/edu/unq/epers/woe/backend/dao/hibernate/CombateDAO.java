package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.combate.Combate;

public class CombateDAO extends AbstractaDAO<Combate> {

    public CombateDAO() {
        super(Combate.class);
    }


    @Override
    Class<Combate> getClase() {
        return Combate.class;
    }
}
