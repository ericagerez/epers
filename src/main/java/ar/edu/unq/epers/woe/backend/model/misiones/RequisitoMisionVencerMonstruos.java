package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RequisitoMisionVencerMonstruos extends RequisitoMision {


    private int cantMonstruosAVencer;

    public RequisitoMisionVencerMonstruos(int cantMonstruosAVencer){ this.cantMonstruosAVencer = cantMonstruosAVencer; }

    public RequisitoMisionVencerMonstruos(){}

    public int getCantMonstruosAVencer() {
        return this.cantMonstruosAVencer;
    }

    @Override
    public boolean cumplioRequisito(Personaje personaje) {
        return personaje.getVecesGanadasContraMonstruo() == this.getCantMonstruosAVencer();
    }

    public void setCantMonstruosAVencer(int cantMonstruosAVencer) {
        this.cantMonstruosAVencer = cantMonstruosAVencer;
    }

}
