package ar.edu.unq.epers.woe.backend.model.eventos;


public class MisionAceptada extends Evento {

   String nombreMision;

    public MisionAceptada(String nombrePersonaje, String nombreLugar, String nombreMision) {
        super(nombrePersonaje, nombreLugar);
        this.nombreMision = nombreMision;
    }

    public MisionAceptada() { }

    public String getNombreMision() {
        return nombreMision;
    }

    public void setNombreMision(String nombreMision) {
        this.nombreMision = nombreMision;
    }

}
