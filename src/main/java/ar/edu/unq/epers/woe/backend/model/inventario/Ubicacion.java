package ar.edu.unq.epers.woe.backend.model.inventario;

public enum Ubicacion {
    CABEZA,
    TORSO,
    PIERNAS,
    PIES,
    MANO_IZQ,
    MANO_DER
}
