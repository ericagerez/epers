package ar.edu.unq.epers.woe.backend.dao.JDBC;

import ar.edu.unq.epers.woe.backend.model.raza.Raza;

public interface RazaDAOJDBC {
    void guardar(Raza raza);
    Raza recuperar(Integer id);


}
