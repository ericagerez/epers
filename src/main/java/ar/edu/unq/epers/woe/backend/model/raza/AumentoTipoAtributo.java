package ar.edu.unq.epers.woe.backend.model.raza;

import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import org.omg.CORBA.PUBLIC_MEMBER;

import javax.persistence.*;

/*
* Esta clase se encarga de saber qué porcentaje aumenta cada atributo cuando un personaje sube de nivel,
* lo cual varia dependiendo del tipo de clase que sea el personaje
* */

@Entity
public class AumentoTipoAtributo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    @Enumerated(EnumType.STRING)
    private TipoAtributo tipoAtributo;

    private Integer porcentajeAAumentar;

    public AumentoTipoAtributo(TipoAtributo tipoAtributo, Integer porcentajeAAumentar) {
        this.tipoAtributo = tipoAtributo;
        this.porcentajeAAumentar = porcentajeAAumentar;
    }

    public AumentoTipoAtributo(){}

    public TipoAtributo getTipoAtributo() {
        return tipoAtributo;
    }

    public Integer getPorcentajeAAumentar() {
        return porcentajeAAumentar;
    }

    public void setTipoAtributo(TipoAtributo tipoAtributo) {
        this.tipoAtributo = tipoAtributo;
    }

    public void setPorcentajeAAumentar(Integer porcentajeAAumentar) {
        this.porcentajeAAumentar = porcentajeAAumentar;
    }
}
