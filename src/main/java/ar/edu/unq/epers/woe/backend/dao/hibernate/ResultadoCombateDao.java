package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.combate.ResultadoCombate;


public class ResultadoCombateDao extends AbstractaDAO<ResultadoCombate> {
    public ResultadoCombateDao(){super(ResultadoCombate.class);}

    @Override
    Class<ResultadoCombate> getClase() {
        return ResultadoCombate.class;
    }
}
