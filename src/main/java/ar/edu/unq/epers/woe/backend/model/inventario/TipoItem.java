package ar.edu.unq.epers.woe.backend.model.inventario;

public enum TipoItem {
    //TODO hay que poner annotations?

    ESPADA,     //Ubicacion en: MANO_DER || MANO_IZQ
    ARCO,      //Ubicacion en: MANO_DER || MANO_IZQ
    FLECHA,   //Ubicacion en: MANO_DER || MANO_IZQ
    ESCUDO,  //Ubicacion en: TORSO
    BOTAS,  //Ubicacion en: PIERNAS || PIES
    CASCO;  //Ubicacion en: CABEZA

    public boolean tieneUbicacionEn(Ubicacion ubicacion){
        switch (this){
            case ESPADA:
                return ubicacion.equals(Ubicacion.MANO_DER) || ubicacion.equals(Ubicacion.MANO_IZQ);
            case ARCO:
                return ubicacion.equals(Ubicacion.MANO_DER) || ubicacion.equals(Ubicacion.MANO_IZQ);
            case FLECHA:
                return ubicacion.equals(Ubicacion.MANO_DER) || ubicacion.equals(Ubicacion.MANO_IZQ);
            case ESCUDO:
                return ubicacion.equals(Ubicacion.TORSO);
            case BOTAS:
                return ubicacion.equals(Ubicacion.PIERNAS) || ubicacion.equals(Ubicacion.PIES);
            case CASCO:
                return ubicacion.equals(Ubicacion.CABEZA);
            default:
                return false;
        }
    }
}
