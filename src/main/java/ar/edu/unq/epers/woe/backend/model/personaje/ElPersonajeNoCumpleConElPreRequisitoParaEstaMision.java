package ar.edu.unq.epers.woe.backend.model.personaje;

public class ElPersonajeNoCumpleConElPreRequisitoParaEstaMision extends RuntimeException {
    public ElPersonajeNoCumpleConElPreRequisitoParaEstaMision( Integer nivel) {
        super("El personaje no cumple la precondicion de tener el nivel " + nivel);
    }
}
