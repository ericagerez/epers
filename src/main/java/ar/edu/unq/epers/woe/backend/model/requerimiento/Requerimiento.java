package ar.edu.unq.epers.woe.backend.model.requerimiento;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Requerimiento {

    @Id
    @GeneratedValue
    private Integer id;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Requisito> requisitos;

    public Requerimiento(List<Requisito> requisitos) {
        this.requisitos = requisitos;
    }

    public Requerimiento(){}

    public boolean satisface(Personaje personaje){
        boolean result = true;
        for(Requisito r : this.requisitos){ result = result && r.satisface(personaje); }
        return result;
    }

    public List<Requisito> getRequisitos(){
        return this.requisitos;
    }

    public void setRequisitos(ArrayList<Requisito> requisitos) {
        this.requisitos = requisitos;
    }
}
