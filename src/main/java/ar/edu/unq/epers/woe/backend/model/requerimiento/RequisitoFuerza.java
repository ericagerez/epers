package ar.edu.unq.epers.woe.backend.model.requerimiento;

import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.Entity;

@Entity
public class RequisitoFuerza extends Requisito {

    public RequisitoFuerza(Integer valor){
        super(valor);
    }

    public RequisitoFuerza(){}

    @Override
    public Boolean satisface(Personaje personaje) {
        return personaje.getAtributo(TipoAtributo.FUERZA).getValor() >= this.valor;

    }

}
