package ar.edu.unq.epers.woe.backend.service.Neo4J;

public class UbicacionMuyLejana extends RuntimeException {
    public UbicacionMuyLejana(){
        super("La ubicacion esta muy lejos para desplazarme");

    }
}
