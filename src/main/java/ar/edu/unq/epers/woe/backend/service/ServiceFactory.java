package ar.edu.unq.epers.woe.backend.service;

import ar.edu.unq.epers.woe.backend.service.data.JDBCDataService;
import ar.edu.unq.epers.woe.backend.service.data.JDBCDataServiceImplementation;
import ar.edu.unq.epers.woe.backend.service.raza.RazaService;
import ar.edu.unq.epers.woe.backend.service.raza.RazaServiceImplementation;

/**
 * Esta clase es un singleton, el cual sera utilizado por equipo de frontend
 * para hacerse con implementaciones a los servicios.
 * 
 * @author Steve Frontend
 * 
 * TODO: Gente de backend, una vez que tengan las implementaciones de sus
 * servicios propiamente realizadas apunten a ellas en los metodos provistos
 * debajo. Gracias!
 */
public class ServiceFactory {
	
	/**
	 * @return un objeto que implementa {@link RazaService}
	 */
	public RazaService getRazaService() {

		return new RazaServiceImplementation();
	}
	
	/**
	 * @return un objeto que implementa {@link JDBCDataService}
	 */
	public JDBCDataService getDataService() {
		return new JDBCDataServiceImplementation();
	}

}
