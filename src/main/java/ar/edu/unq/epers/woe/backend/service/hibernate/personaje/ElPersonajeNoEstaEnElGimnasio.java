package ar.edu.unq.epers.woe.backend.service.hibernate.personaje;

public class ElPersonajeNoEstaEnElGimnasio extends RuntimeException {
    public ElPersonajeNoEstaEnElGimnasio(){
        super("El personaje no se encuentra en el gimnasio");
    }
}
