package ar.edu.unq.epers.woe.backend.service.hibernate.leaderboard;

import ar.edu.unq.epers.woe.backend.dao.hibernate.ClaseDAO;
import ar.edu.unq.epers.woe.backend.dao.hibernate.PersonajeDAO;
import ar.edu.unq.epers.woe.backend.dao.hibernate.RazaDAO;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;

import java.util.List;

import static ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner.run;

public class LeaderboardImplService implements LeaderboardService {

    PersonajeDAO personajeDAO;
    RazaDAO razaDAO;
    ClaseDAO claseDAO;

    public LeaderboardImplService(PersonajeDAO personajeDAO, RazaDAO razaDAO, ClaseDAO claseDAO){
        this.personajeDAO=personajeDAO;
        this.razaDAO=razaDAO;
        this.claseDAO= claseDAO;
    }

    public void guardarPersonaje(Personaje personaje) {
        run(() -> {
            this.personajeDAO.guardar(personaje);
        });
    }

    public Personaje recuperarPersonaje(Integer personajeId) {
        return run(() -> this.personajeDAO.recuperar(personajeId));
    }
    public void guardarRaza(Raza raza) {
        run(() -> {
            this.razaDAO.guardar(raza);
        });
    }

    public Raza recuperarRaza(Integer razaId) {
        return run(() -> this.razaDAO.recuperar(razaId));
    }

    public void guardarClase(Clase clase) {
        run(() -> {
            this.claseDAO.guardar(clase);
        });
    }

    public Clase recuperarClase(Integer claseId) {
        return run(() -> this.claseDAO.recuperar(claseId));
    }


    @Override
    public List<Personaje> campeones() {
    /**
     * Retorna los diez personajes que ganaron mas batallas contra otros personajes ordenado de mayor a menor.**/

        return run(()->
                this.personajeDAO.getCampeones());
    }

    @Override
    public Raza razaLider() {
    /**
     * Retorna la raza que tenga mas batallas ganadas contra otros personajes.**/

        return run(()->
                this.razaDAO.getRazaLider());
    }

    @Override
    public Clase claseLider() {
    /**
     * Retorna la clase que tenga mas batallas ganadas contra otros personajes.**/

        return run(()->
                this.claseDAO.getClaseLider());
    }

    @Override
    public List<Raza> razasPopulares() {
    /**
     * Retorna aquellas diez razas mas populares, o sea, aquellas que tengan mas personajes de la misma raza.**/

        return run(()->
                this.razaDAO.getRazasPopulares());
    }

    @Override
    public List<Clase> clasesPopulares() {
    /**
     * Retorna aquellas diez clases mas populares, o sea, aquellas que tengan mas personajes de la misma clase.**/

        return run(()->
                this.claseDAO.getClasesPopulares());
    }

    @Override
    public Personaje masFuerte() {
    /**
     * Retorna el personaje que produzca mas daño.**/

        return run(()->
                this.personajeDAO.getMasFuerte());
    }
}
