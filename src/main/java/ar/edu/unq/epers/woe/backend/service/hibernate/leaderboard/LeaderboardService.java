package ar.edu.unq.epers.woe.backend.service.hibernate.leaderboard;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;

import java.util.List;

public interface LeaderboardService {

    List<Personaje> campeones();

    Raza razaLider();

    Clase claseLider();

    List<Raza> razasPopulares();

    List<Clase> clasesPopulares();

    Personaje masFuerte();
}
