package ar.edu.unq.epers.woe.backend.model.inventario;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Entity
public class Inventario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Slot> slots = new ArrayList<>();


    public Inventario(){
        slots.add(new Slot(Ubicacion.CABEZA));
        slots.add(new Slot(Ubicacion.TORSO));
        slots.add(new Slot(Ubicacion.PIERNAS));
        slots.add(new Slot(Ubicacion.PIES));
        slots.add(new Slot(Ubicacion.MANO_IZQ));
        slots.add(new Slot(Ubicacion.MANO_DER));
    }

    public void setSlots(ArrayList<Slot> slots) { this.slots = slots; }

    public void equipar(Item item, Personaje personaje) {
        Slot slotCorrespondiente = this.getSlotDe(item.getUbicacion());
        if(slotCorrespondiente.tieneItem()){
            intercambiarItems(personaje, item, slotCorrespondiente.getItem());
        }
        else {
            slotCorrespondiente.agregarItem(item);
        }
    }

    public void intercambiarItems(Personaje personaje, Item itemNuevo, Item itemViejo) {
        this.addItem(itemNuevo);
        personaje.guarItemEnMochila(itemViejo);
    }

    public Slot getSlotDe(Ubicacion u){

        return this.getSlots()
                .stream()
                .filter(s -> u == s.getUbicacion()).collect(toList()).get(0);
    }

    public void addItem(Item item) {
        this.getSlotDe(item.getUbicacion()).agregarItem(item);
    }

    public List<Slot> getSlots(){ return this.slots; }


    public Item getItemConUbicacion(Ubicacion ubicacion) {
        return this.getSlotDe(ubicacion).getItem();
    }

    public ArrayList<Item> getAllItems(){
        ArrayList<Item> items = new ArrayList<>();
        this.getSlots().stream().forEach(s -> items.add(s.getItem()));
        return items;
    }

    public Integer getId() {
        return id;
    }
}
