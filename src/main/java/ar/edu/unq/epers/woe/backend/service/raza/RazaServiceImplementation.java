package ar.edu.unq.epers.woe.backend.service.raza;

import ar.edu.unq.epers.woe.backend.dao.JDBC.JDBCRazaDAOJDBC;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;

import java.util.List;

public class RazaServiceImplementation implements RazaService {

   public JDBCRazaDAOJDBC razaDAO = new JDBCRazaDAOJDBC();

    @Override
    public void crearRaza(Raza raza) {
        try {
            razaDAO.guardar(raza);
        } catch (RuntimeException e) {
            throw new NoSePuedeCrearOtraaRazaYaExistenteException(raza);
        }

    }

    @Override
    public Raza getRaza(Integer razaId) {

        Raza raza= razaDAO.recuperar(razaId);
        if (raza == null) {
            throw new RazaNoExistente(razaId);
        }


        return raza;
    }


    @Override
    public List<Raza> getAllRazas() {

        return razaDAO.recuperarTodos();

    }

    @Override
    public Personaje crearPersonaje(Integer razaId, String nombrePersonaje, Clase clasePersonaje) {

        Raza raza =this.getRaza(razaId);
        Personaje unPersonaje= raza.crearPersonaje(nombrePersonaje,clasePersonaje);

        razaDAO.actualizar(raza);
        return unPersonaje;

    }



}
