package ar.edu.unq.epers.woe.backend.model.requerimiento;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.Entity;

@Entity
public class RequisitoNivel extends Requisito {

    public RequisitoNivel(int valor){
        super(valor);
    }

    public RequisitoNivel(){}

    @Override
    public Boolean satisface(Personaje personaje) {
        return personaje.getNivel() >= this.valor;
    }


}