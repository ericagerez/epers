package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RecompensaMonedas extends Recompensa {

    private int monedas;

    public RecompensaMonedas(int monedas) {
        this.monedas = monedas;
    }

    public RecompensaMonedas(){}

    @Override
    public void darRecompensaCorrespondiente(Personaje personaje) {
        personaje.setBilletera(personaje.getBilletera() + this.monedas);
    }

    public void setMonedas(int monedas) {
        this.monedas = monedas;
    }
}