package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.misiones.Mision;

public class MisionDAO extends AbstractaDAO<Mision> {
    public MisionDAO() {
        super(Mision.class);
    }


    @Override
    Class<Mision> getClase() {
        return Mision.class;
    }
}
