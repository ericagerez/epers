package ar.edu.unq.epers.woe.backend.model.lugar;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;

import javax.persistence.*;

@Entity
public class Gimnasio extends Lugar {

    //El id y los setters lo hereda
    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.EAGER)
    Rival rival;

    public Gimnasio(String nombreLugar, Rival rival) {
        super(nombreLugar);
        this.rival = rival;
    }

    public Gimnasio(){}

    public boolean esGimnasio(){
        return true;
    }

    public void combatir(Personaje personaje){
        personaje.combatirContra(rival);
    }



}