package ar.edu.unq.epers.woe.backend.service.data;

import ar.edu.unq.epers.woe.backend.model.raza.Raza;


public interface JDBCDataService {

	void eliminarDatos();
	void crearSetDatosIniciales();

}
