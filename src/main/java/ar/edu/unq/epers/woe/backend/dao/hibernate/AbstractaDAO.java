package ar.edu.unq.epers.woe.backend.dao.hibernate;


import ar.edu.unq.epers.woe.backend.service.hibernate.runner.SessionFactoryProvider;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.io.Serializable;
import java.util.List;

abstract class AbstractaDAO<T> implements DAO<T> {

    private Class<T> entityType;

    public AbstractaDAO(Class<T> entityType){
        this.entityType = entityType;
    }

    /** Con este método se puede generalizar completamente la clase AbstractaDAO para todos los DAOs **/
    abstract Class<T> getClase();

    public void guardar(T object) {
        Session session = TransactionRunner.getCurrentSession();
        session.save(object);
    }

    /**id es de tipo Serializable porque lo exije el get de session **/
    public T recuperar(Serializable id) {
        Session session = TransactionRunner.getCurrentSession();
        return session.get(this.getClase(), id);
    }

    @Override
    public List<T> recuperarTodos() {
        Session session = TransactionRunner.getCurrentSession();

        String hql = "from " + this.getClase().getName() + " e";

        Query<T> query = session.createQuery(hql, this.getClase());
        return query.getResultList();
    }

    public void actualizar(T anObject) {
        Session session = TransactionRunner.getCurrentSession();
        session.update(anObject);
    }

    public void borrarTodo(){
        SessionFactoryProvider.destroy();
    }

}
