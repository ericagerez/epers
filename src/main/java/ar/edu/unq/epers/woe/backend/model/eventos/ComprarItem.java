package ar.edu.unq.epers.woe.backend.model.eventos;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import java.time.LocalDateTime;

public class ComprarItem extends Evento {

    private String item;
    private int precio;

    public ComprarItem() {}

    public ComprarItem(String nombrePersonaje, String nombreLugar, String idItem, int precio) {
        super(nombrePersonaje, nombreLugar);
        this.item = idItem;
        this.precio = precio;
    }

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public int getPrecio() {
        return precio;
    }

    public void setPrecio(int precio) {
        this.precio = precio;
    }

}
