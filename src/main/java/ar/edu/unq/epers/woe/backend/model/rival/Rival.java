package ar.edu.unq.epers.woe.backend.model.rival;

import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.misiones.Mision;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.io.Serializable;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Rival implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    public Rival() {
    }

    public abstract double atacarAlOponente(Rival rival);

    public abstract double getDefensa();

    public abstract double getVida();

    public abstract boolean esPersonaje();

    public abstract void setCantPuntosDeExperiencia(int i);

    public abstract int getCantPuntosDeExperiencia();

    public abstract int getNivel();

    public Integer getId() { return id; }

    public abstract double getDannoPersonaje();

    public abstract Lugar getLugar();

    public abstract void setCombatesGanados(int i);




}