package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner;
import org.hibernate.Session;
import org.hibernate.query.Query;


public class LugarDAO extends AbstractaDAO<Lugar> {
    public LugarDAO() {
        super(Lugar.class);
    }


    @Override
    Class<Lugar> getClase() {
        return Lugar.class;
    }

    public Lugar recuperarPorNombre(String nombre){
        Session session = TransactionRunner.getCurrentSession();
        String hql ="select l "
                +"from Lugar l "
                +"where l.nombreLugar = '"+nombre + "'";

        Query<Lugar> query = session.createQuery(hql, Lugar.class);
        query.setMaxResults(1);

        return query.getSingleResult();

    }


}
