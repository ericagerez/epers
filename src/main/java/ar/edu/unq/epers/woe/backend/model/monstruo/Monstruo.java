package ar.edu.unq.epers.woe.backend.model.monstruo;

import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;

import javax.persistence.*;

@Entity
public class Monstruo extends Rival {

    private double vida;
    private double danno;

    @Enumerated(EnumType.STRING)
    private TipoMonstruo tipo;

    private double defensa;

    public Monstruo(double vida, double danno, double defensa, TipoMonstruo tipo){
        super();
        this.setVida(vida);
        this.setDanno(danno);
        this.setDefensa(defensa);
        this.setTipoMonstruo(tipo);
    }

    public Monstruo(){ }

    /****************************** SETTERS ******************************/

    private void setVida(double vida) {
        this.vida=vida;
    }
    public void setDanno(double danno) {
        this.danno = danno;
    }
    public void setTipoMonstruo(TipoMonstruo tipo) {
        this.tipo = tipo;
    }
    public void setDefensa(double defensa){this.defensa = defensa;}
    public void setCantPuntosDeExperiencia(int i) { }
    public void setCombatesGanados(int i){}


    /****************************** GETTERS ******************************/

    public double getVida() {
        return vida;
    }
    public double getDanno() {
        return danno;
    }
    public TipoMonstruo getTipo() {
        return tipo;
    }
    public double getDefensa(){return defensa;}
    public int getCantPuntosDeExperiencia() { return 0;}
    public int getNivel() { return 0; }
    public  Lugar getLugar(){return null;}
    

    /****************************** OTRAS ACCIONES ******************************/

    public double atacarAlOponente(Rival rival) {
        double dannoDelAtacante= this.getDanno();
        double defensaRival= rival.getDefensa();
        double dannoRecibidoRival= dannoDelAtacante - defensaRival;
        return dannoRecibidoRival;
    }

    public boolean esPersonaje() {
        return false;
    }

    public  double getDannoPersonaje(){
        return this.getDanno();
    }

}
