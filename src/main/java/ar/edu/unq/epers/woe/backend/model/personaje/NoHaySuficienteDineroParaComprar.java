package ar.edu.unq.epers.woe.backend.model.personaje;

public class NoHaySuficienteDineroParaComprar extends RuntimeException {
    public NoHaySuficienteDineroParaComprar(Integer costoDeVenta, int billetera) {
        super("Usted no tiene suficiente dinero, tiene:"+ billetera + "y quiere gastar"+ costoDeVenta);
    }
}
