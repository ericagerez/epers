package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Mision {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column
    private String nombre;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private RequisitoMision requisitoMision;

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Recompensa recompensa;
/// hay recompensas complejas en donde puede hacer mas de una recompensa

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private PreCondicionParaAceptacionMision preCondicionParaAceptacionMision;

    @ManyToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private List<Mision> misionesHijas;

    public Mision(String nombre,RequisitoMision requisitoMision, Recompensa recompensa){
        this.nombre = nombre;
        this.requisitoMision = requisitoMision;
        this.recompensa = recompensa;

        /// cada ves que una mision se cree hay que setearle a mano las misionesHijas
        this.misionesHijas = new ArrayList<>();

        // en primera instancia la precondificion es que el personaje tenga nivel 1
        this.preCondicionParaAceptacionMision = new PreCondicionParaAceptacionMision(1);
    }


    public Mision(){}
    public String getNombre(){return  this.nombre;}
    public void  setNombre(String nombre){this.nombre = nombre;}

    public  RequisitoMision getRequisito(){
        return this.requisitoMision;
    }

    public Recompensa getRecompensa() { return recompensa; }

    public Integer getId(){return this.id;}

    public void update(Personaje personaje){
        if (this.getRequisito().cumplioRequisito(personaje)) {
            getRecompensa().darRecompensaCorrespondiente(personaje);
            personaje.eliminarMisionRealizada(this);
        }
    }

    public void setRequisitoMision(RequisitoMision requisitoMision) {
        this.requisitoMision = requisitoMision;
    }

    public void setRecompensa(Recompensa recompensa) {
        this.recompensa = recompensa;
    }

    public List<Mision> getMisionesHijas(){return this.misionesHijas;}
    public void setMisionesHijas(List<Mision> misionesHijas){
        this.misionesHijas = misionesHijas;
    }

    public PreCondicionParaAceptacionMision getPreCondicionParaAceptacionMision(){return  this.preCondicionParaAceptacionMision;}
    public void setPreCondicionParaAceptacionMision(PreCondicionParaAceptacionMision preCondicionParaAceptacionMision){
        this.preCondicionParaAceptacionMision  = preCondicionParaAceptacionMision;
    }
}
