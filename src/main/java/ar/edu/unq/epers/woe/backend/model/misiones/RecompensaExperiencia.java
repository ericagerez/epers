package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class RecompensaExperiencia extends Recompensa {



    private int experiencia;

    public RecompensaExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }

    public RecompensaExperiencia(){}

    @Override
    public void darRecompensaCorrespondiente(Personaje personaje) {
        personaje.
                setCantPuntosDeExperiencia(
                        personaje.getCantPuntosDeExperiencia() +
                                this.experiencia);
    }

    public void setExperiencia(int experiencia) {
        this.experiencia = experiencia;
    }
}