package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner;
import org.hibernate.Session;
import org.hibernate.query.Query;

import java.util.List;

public class ClaseDAO extends AbstractaDAO<Clase> {
    public ClaseDAO() {
        super(Clase.class);
    }

    public List<Clase> getClasesPopulares() {
        Session session = TransactionRunner.getCurrentSession();
        String hql = "select clase"
                +" from Clase clase"
                + " order by clase.cantDePersonajes desc";

        Query<Clase> query = session.createQuery(hql,  Clase.class);
        query.setMaxResults(10);

        return query.getResultList();
    }

    public Clase getClaseLider(){
        Session session = TransactionRunner.getCurrentSession();

        String hql = "select p.clase "
                +"from Personaje p "
                +"group by p.clase "
                +"order by sum(p.combatesGanados) desc";

        Query<Clase> query = session.createQuery(hql,  Clase.class);
        query.setMaxResults(1);
        return query.getSingleResult();
    }


    @Override
    Class<Clase> getClase() {
        return Clase.class;
    }

}
