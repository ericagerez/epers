package ar.edu.unq.epers.woe.backend.service.mongo;

import ar.edu.unq.epers.woe.backend.dao.Neo4j.LugarDaoNeo4j.LugarDaoNeo4j;
import ar.edu.unq.epers.woe.backend.dao.mongo.EventoDAO;
import ar.edu.unq.epers.woe.backend.model.eventos.Evento;
import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.service.hibernate.lugar.LugarImplService;

import java.util.ArrayList;
import java.util.List;

public class FeedServiceImpl {
   private EventoDAO eventoDAO;
    private LugarDaoNeo4j lugarDAO;

    public FeedServiceImpl() {
        eventoDAO = new EventoDAO();
        lugarDAO = new LugarDaoNeo4j();

    }

    /*
     * Devuelve la lista de eventos que involucren al personaje provisto,
     * conteniendo primero los eventos mas recientes
     */

    public List<Evento> feedPersonaje(String nombrePersonaje){

        return eventoDAO.getByPersonaje(nombrePersonaje);
    }


    /*
     * Devuelve los eventos que ocurrieron en el lugar mas todos los eventos de los
     * lugares que esten conectados con el mismo, conteniendo primero los eventos
     * mas recientes
     */

    public List<Evento> feedLugar(String nombreLugar){
            List<String> lugares = lugarDAO.conectadosCon(nombreLugar);
            lugares.add(nombreLugar);

            List<Evento> eventos = eventoDAO.getByLugares(lugares);
            return eventos;


    }
}
