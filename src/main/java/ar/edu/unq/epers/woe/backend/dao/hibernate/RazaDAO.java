package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner;
import org.hibernate.Session;
import org.hibernate.query.Query;
import java.util.List;

public class RazaDAO extends AbstractaDAO<Raza> {

    public RazaDAO() {
        super(Raza.class);
    }

    public Raza getRazaLider(){
        Session session = TransactionRunner.getCurrentSession();
        String hql = "select p.raza from Personaje p group by p.raza order by sum(p.combatesGanados) desc";
        Query<Raza> query = session.createQuery(hql,  Raza.class);
        query.setMaxResults(1);
        return query.getSingleResult();
    }

    public List<Raza> getRazasPopulares(){
        Session session = TransactionRunner.getCurrentSession();

        String hql = "select r"
                    +" from Raza r"
                    + " order by r.cantidadPersonajes desc";
        Query<Raza> query = session.createQuery(hql,  Raza.class);
        query.setMaxResults(10);
        return query.getResultList();
    }

    @Override
    Class<Raza> getClase() {
        return Raza.class;
    }

}
