package ar.edu.unq.epers.woe.backend.dao.Neo4j.LugarDaoNeo4j;

import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import org.neo4j.driver.v1.*;

import java.util.ArrayList;
import java.util.List;

public class LugarDaoNeo4j {

    private Driver driver;
    private String labelNombre;
    private String labelRelacion;
    private String labelTipoCamino;
    private String labelCostoCamino;
    private String labelTipoNodo;


    public LugarDaoNeo4j() {
        this.driver = GraphDatabase.driver("bolt://localhost:7687", AuthTokens.basic("neo4j", "password"));
        this.labelNombre = "nombre";
        this.labelRelacion = "conectadoCon";
        this.labelTipoCamino = "tipoCamino";
        this.labelCostoCamino = "costoCamino";
        this.labelTipoNodo = "Lugar";

    }

    /**
     * Crea un lugar
     *
     * @param lugar
     */
    public void create(Lugar lugar) {

        Session session = this.driver.session();
        if (existeLugar(lugar.getNombre())) {
            throw new RuntimeException("Ya existe Lugar con ese nombre");
        }
        try {
            String query = "MERGE (n:%s { nombre: {elNombre} }) SET n.%s = {elNombre}";
            query = String.format(query, this.labelTipoNodo, this.labelNombre);
            session.run(query, Values.parameters("elNombre", lugar.getNombre()));
        } finally {
            session.close();
        }
    }

    public Boolean existeLugar(String nombreLugar) {
        Session session = this.driver.session();
        try {
            String query = "MATCH (l:%s { %s: {elNombre} }) " +
                    "RETURN l";
            query = String.format(query, this.labelTipoNodo, this.labelNombre);
            StatementResult result = session.run(query, Values.parameters("elNombre", nombreLugar));
            return result.list().size() == 1;
        } finally {
            session.close();
        }
    }


    /**
     * Conexion entre dos lugares
     *
     * @param lugarPartida
     * @param lugarLlegada
     * @param tipoCamino
     */
    public void crearRuta(String lugarPartida, String lugarLlegada, String tipoCamino) {

        Session session = this.driver.session();
        try {
            Integer costoCamino = null;
            switch (tipoCamino) {
                case "terrestre":
                    costoCamino = 1;
                    break;
                case "maritimo":
                    costoCamino = 2;
                    break;
                case "aereo":
                    costoCamino = 5;
                    break;
            }
            String query = "MATCH (p:%s { %s: {elNombreP} }) " +
                    "MATCH (l:%s { %s: {elNombreL} }) " +
                    "MERGE (p)-[:%s {%s: {tipoCamino}, %s: {costoCamino}}]->(l)";
            query = String.format(query, this.labelTipoNodo, this.labelNombre, this.labelTipoNodo, this.labelNombre,
                    this.labelRelacion, this.labelTipoCamino, this.labelCostoCamino);
            session.run(query, Values.parameters("elNombreP", lugarPartida,
                    "elNombreL", lugarLlegada, "tipoCamino", tipoCamino, "costoCamino", costoCamino));
        } finally {
            session.close();
        }


    }

    public void tearDown() {
        // eliminar datos
        Session session = this.driver.session();
        try {
            String query = "MATCH (n) DETACH DELETE n";
            session.run(query);
        } finally {
            session.close();
        }
    }


    /**
     * Establece la cantidad de Lugares conectados a un Lugar por un tipo de tramo
     */
    public List<String> conectados(String nombreLugar, String tipoCamino) {
        /// conectadosCon
        Session session = this.driver.session();
        try {
            List<String> res = new ArrayList<String>();
            String query = "MATCH(l:%s)-[r:%s]->(lugar) " +
                    "WHERE l.%s = {elNombre} and r.%s = {tipoCamino} " +
                    "RETURN lugar.%s";
            query = String.format(query, this.labelTipoNodo, this.labelRelacion, this.labelNombre, this.labelTipoCamino, this.labelNombre);
            StatementResult result = session.run(query, Values.parameters("elNombre", nombreLugar,
                    "tipoCamino", tipoCamino));
            for (Record r : result.list()) {
                res.add(r.get(0).asString());
            }
            return res;
        } finally {
            session.close();
        }


    }

    /**
     * Devuelve la lista de las Lugares a recorrer para que la ruta sea la mas corta
     *
     * @param lugarPartida
     * @param lugarLlegada
     * @return
     */
    public Integer costoTrayectoMasCorto(String lugarPartida, String lugarLlegada) {
        Session session = this.driver.session();
        try {
            String query = "MATCH (p:%s { %s: {elNombreP} }) " +
                    "MATCH (l:%s { %s: {elNombreL} }) " +
                    "MATCH ds=shortestPath((p)-[:%s*]->(l)) " +
                    "RETURN reduce(costoTotal=0, rels in relationships(ds) |  costoTotal+rels.%s)";
            query = String.format(query, this.labelTipoNodo, this.labelNombre, this.labelTipoNodo, this.labelNombre,
                    this.labelRelacion, this.labelCostoCamino);
            return session.run(query, Values.parameters("elNombreP", lugarPartida,
                    "elNombreL", lugarLlegada)).next().get(0).asInt();
        } finally {
            session.close();
        }
    }

    /**
     * Devuelve un booleano diciendo si existe una ruta que conecte 2 lugares
     *
     * @param lugarPartida
     * @param lugarLlegada
     * @return
     */
    public boolean existeRuta(String lugarPartida, String lugarLlegada) {
        // exite camino entre

        Session session = this.driver.session();
        try {
            String query = "MATCH(l:%s {%s: {elNombreP}})-[:%s*]->(d:%s {%s: {elNombreL}}) " +
                    "RETURN d";
            query = String.format(query, this.labelTipoNodo, this.labelNombre, this.labelRelacion, this.labelTipoNodo, this.labelNombre);
            StatementResult result = session.run(query, Values.parameters("elNombreP", lugarPartida,
                    "elNombreL", lugarLlegada));
            return result.list().size() >= 1;
        } finally {
            session.close();
        }

    }

    public Integer costoDelTrayectoMasBarato(String lugarPartida, String lugarLlegada) {
        Session session = this.driver.session();
        try {
            String query = "MATCH (p:%s { %s: {elNombreP} }) " +
                    "MATCH (l:%s { %s: {elNombreL} }) " +
                    "MATCH ds=(p)-[:%s*]->(l) " +
                    "RETURN reduce(costoTotal=0, r in relationships(ds) | costoTotal+r.%s) AS costoTotal " +
                    "order by costoTotal limit 1";
            query = String.format(query, this.labelTipoNodo, this.labelNombre, this.labelTipoNodo, this.labelNombre,
                    this.labelRelacion, this.labelCostoCamino);
            return session.run(query, Values.parameters("elNombreP", lugarPartida,
                    "elNombreL", lugarLlegada)).next().get(0).asInt();
        } finally {
            session.close();
        }

    }

    public List<String> conectadosCon(String nombreLugar) {
        Session session = this.driver.session();
        try {
            List<String> res = new ArrayList<String>();
            String query = "MATCH(l:%s)-[:%s]->(lugar) " +
                    "WHERE l.%s = {elNombre} " +
                    "RETURN lugar.%s";
            query = String.format(query, this.labelTipoNodo, this.labelRelacion, this.labelNombre, this.labelNombre);
            StatementResult result = session.run(query, Values.parameters("elNombre", nombreLugar));
            for (Record r : result.list()) {
                res.add(r.get(0).asString());
            }
            return res;
        } finally {
            session.close();
        }
    }
}
