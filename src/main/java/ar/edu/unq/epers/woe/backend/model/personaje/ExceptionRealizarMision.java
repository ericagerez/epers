package ar.edu.unq.epers.woe.backend.model.personaje;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;

public class ExceptionRealizarMision extends RuntimeException {

    //TODO hay que poner annotations?

    public ExceptionRealizarMision() {
        super("El personaje debe estar en una Taberna para realizar una mision y la mision debe estar Aceptada");
    }
}