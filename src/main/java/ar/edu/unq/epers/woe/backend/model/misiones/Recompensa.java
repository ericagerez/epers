package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;


import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Recompensa {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


       public  abstract void darRecompensaCorrespondiente(Personaje personaje);

       public Recompensa(){}

       public Integer getId(){
           return  this.id;
       }
    }