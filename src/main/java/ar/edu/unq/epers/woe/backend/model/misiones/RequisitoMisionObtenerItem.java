package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;

@Entity
public class RequisitoMisionObtenerItem extends RequisitoMision {


    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Item itemRequisito;

    public RequisitoMisionObtenerItem(Item itemRequisito){
        this.itemRequisito = itemRequisito;
    }

    public RequisitoMisionObtenerItem(){}

    public Item getItemRequisito(){
        return this.itemRequisito;
    }

    @Override
    public boolean cumplioRequisito(Personaje personaje) {
        return personaje.contieneItem(getItemRequisito());
    }

    public void setItemRequisito(Item itemRequisito) {
        this.itemRequisito = itemRequisito;
    }

}
