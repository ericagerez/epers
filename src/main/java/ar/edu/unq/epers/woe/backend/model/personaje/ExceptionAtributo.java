package ar.edu.unq.epers.woe.backend.model.personaje;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;

public class ExceptionAtributo extends RuntimeException {

    //TODO hay que poner annotations?

    public ExceptionAtributo(Atributo atributo){
        super("Este atributo del tipo "+ atributo.getTipo() +" ya esta asignado");
    }
}
