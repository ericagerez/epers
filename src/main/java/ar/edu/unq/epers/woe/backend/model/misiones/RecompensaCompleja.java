package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.List;


@Entity
public class RecompensaCompleja extends Recompensa {



    @OneToMany
    private List<Recompensa> recompensas;

    public RecompensaCompleja(List<Recompensa> recompensas){
        this.recompensas = recompensas;
    }

    public RecompensaCompleja(){}

    @Override
    public void darRecompensaCorrespondiente(Personaje personaje) {
        this.recompensas.stream().forEach(r -> r.darRecompensaCorrespondiente(personaje));
    }


    public void setRecompensas(List<Recompensa> recompensas) {
        this.recompensas = recompensas;
    }
}
