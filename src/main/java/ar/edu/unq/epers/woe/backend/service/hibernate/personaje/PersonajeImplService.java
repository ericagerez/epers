package ar.edu.unq.epers.woe.backend.service.hibernate.personaje;

import ar.edu.unq.epers.woe.backend.dao.hibernate.*;

import ar.edu.unq.epers.woe.backend.dao.mongo.EventoDAO;
import ar.edu.unq.epers.woe.backend.model.combate.ResultadoCombate;
import ar.edu.unq.epers.woe.backend.model.eventos.Ganador;
import ar.edu.unq.epers.woe.backend.model.eventos.MisionCompletada;
import ar.edu.unq.epers.woe.backend.model.eventos.Perdedor;
import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.lugar.Gimnasio;
import ar.edu.unq.epers.woe.backend.model.misiones.Mision;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.service.mongo.FeedServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner.run;


public class PersonajeImplService implements PersonajeService{


    private PersonajeDAO personajeDAO;
    private ItemDAO itemDAO;
    private ClaseDataDAO dataDAO;
    private GimnasioDao gimnasioDAO;
    private ResultadoCombateDao resultadoCombateDao;
    private EventoDAO eventoDAO;
    private FeedServiceImpl feedService;

    public PersonajeImplService(PersonajeDAO personajeDAO, ItemDAO itemDAO, ClaseDataDAO dataDAO, GimnasioDao gimnasioDAO, ResultadoCombateDao resultadoCombateDao, EventoDAO eventoDAO ){
        this.personajeDAO = personajeDAO;
        this.itemDAO      = itemDAO;
        this.dataDAO      = dataDAO;
        this.gimnasioDAO  = gimnasioDAO;
        this.resultadoCombateDao = resultadoCombateDao;
        this.eventoDAO = eventoDAO;
    }

    public void setFeedService(FeedServiceImpl feedService){
        this.feedService = feedService;
    }


    public void guardarItem(Item item) {
        run(() -> {
            this.itemDAO.guardar(item);
        });
    }

    public void guardarPersonaje(Personaje personaje) {
        run(() -> {
            this.personajeDAO.guardar(personaje);
        });
    }

    public Personaje recuperarPersonaje(Integer personajeId) {
        return run(() -> this.personajeDAO.recuperar(personajeId));
    }

    public Item recuperarItem(Integer item) {
        return run(() -> this.itemDAO.recuperar(item));
    }

    public ResultadoCombate recuperarResultado(Integer idResultadoCombate) {
        return run(() ->
                this.resultadoCombateDao.recuperar(idResultadoCombate));
    }

        public void guardarGimnasio (Gimnasio gimnasio){
            run(() -> {
                this.gimnasioDAO.guardar(gimnasio);
            });
        }
        public Gimnasio recuperarGimnasio (Integer gimnasioId){
            return run(() -> this.gimnasioDAO.recuperar(gimnasioId));
        }



        public void guardarResultadoCombate (ResultadoCombate resultadoCombate){
            run(() -> {
                this.resultadoCombateDao.guardar(resultadoCombate);
            });
        }

        @Override
        public void equipar (Integer idPersonaje, Integer idItem){
            run(() -> {
                Personaje personaje = this.personajeDAO.recuperar(idPersonaje);
                Item item = this.itemDAO.recuperar(idItem);
                personaje.equiparItemAlInventario(item);

            });
        }

        @Override
        public ResultadoCombate combatir (Integer idPersonaje, Integer idPersonaje1){
            return run(() -> {
                Personaje personaje = this.personajeDAO.recuperar(idPersonaje);
                Personaje personaje1 = this.personajeDAO.recuperar(idPersonaje1);

                ResultadoCombate resultado;

                if (personaje.getLugar().esGimnasio()) {
                    List<String>l1 = misionesCumplidasPor(personaje);
                    List<String>l2 = misionesCumplidasPor(personaje1);

                    resultado = personaje.combatirContra(personaje1);
                    personajeDAO.actualizar(personaje);
                    generarEventosSiCorresponde(resultado,l1,l2);
                    return resultado;
                } else {
                    throw new ElPersonajeNoEstaEnElGimnasio();
                }
            });
        }



   public List<String> misionesCumplidasPor(Personaje pj) {
        List<String> res = new ArrayList<>();
        res.add(pj.getNombre());
        List<String>nombreDeLasMisiones = new ArrayList<>();
        for(Mision m: pj.getMisionesCompletadas()) {
            nombreDeLasMisiones.add(m.getNombre());

        }
        res.addAll(nombreDeLasMisiones);
        return res;
    }

    private void generarEventosSiCorresponde(ResultadoCombate resultadoCombate, List<String> l1, List<String> l2) {
        Personaje g = (Personaje) resultadoCombate.getGanador();
        Personaje p = (Personaje) resultadoCombate.getPerdedor();
        this.eventoDAO.save(new Ganador(g.getNombre(), g.getLugar().getNombre(), p.getNombre(), p.getClase().getNombre(),
                g.getNombre(), p.getRaza().getNombre(), g.getRaza().getNombre()));
        List<String> mis = new ArrayList<>(p.getMisionesCompletadasNombres());

        if(g.getNombre().equals(l1.get(0)) && g.getMisionesCompletadasNombres().size() > (l1.size()-1)) {
            mis.removeAll(l1);
            this.eventoDAO.save(new MisionCompletada(g.getNombre(), g.getLugar().getNombre(), mis.iterator().next()));
        } else if(g.getNombre().equals(l2.get(0)) && g.getMisionesCompletadasNombres().size() > (l2.size()-1)) {
            mis.removeAll(l2);
            this.eventoDAO.save(new MisionCompletada(g.getNombre(), g.getLugar().getNombre(), mis.iterator().next()));
        }
    }


}

