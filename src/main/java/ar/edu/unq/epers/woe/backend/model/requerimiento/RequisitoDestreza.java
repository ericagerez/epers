package ar.edu.unq.epers.woe.backend.model.requerimiento;

import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.Entity;

@Entity
public class RequisitoDestreza extends Requisito {


    public RequisitoDestreza(Integer valor) {
        super(valor);
    }

    public RequisitoDestreza(){}

    @Override
    public Boolean satisface(Personaje personaje) {
        return personaje.getAtributo(TipoAtributo.DESTREZA).getValor()  >= this.valor;

    }
}
