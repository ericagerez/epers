package ar.edu.unq.epers.woe.backend.model.raza;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Clase {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	private String nombre;

	private int cantDePersonajes;

	@OrderColumn(name="orderIndex")
	@ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<AumentoTipoAtributo> aumentoTipoAtributos = new ArrayList<>();

	public Clase(String nombre) {
		this.nombre = nombre;
		this.setCantDePersonajes(cantDePersonajes);
	}

	public Clase() {
	}

	public void aumentarAtributo(Personaje personaje, TipoAtributo tipoAtributo) {

		Atributo atributoAModificar = personaje.getAtributo(tipoAtributo);
		double valorActual = atributoAModificar.getValor();
		Integer porcentajeAAumentar = getAumentoTipoAtributo(tipoAtributo).getPorcentajeAAumentar();
		Integer decimal = porcentajeAAumentar / 100; //paso a decimal el double porcentajeAAumentar
		// que es considerado como porcentual
		atributoAModificar.setValor(valorActual + valorActual * decimal);
	}

	public AumentoTipoAtributo getAumentoTipoAtributo(TipoAtributo tipoAtributo) {
		return this.aumentoTipoAtributos.stream()
				.filter(a -> a.getTipoAtributo() == tipoAtributo)
				.collect(Collectors.toList()).get(0);
	}

	public void addAumentoTipoAtributo(AumentoTipoAtributo a) {
		this.aumentoTipoAtributos.add(a);
	}

	public void setAumentoTipoAtributo(ArrayList<AumentoTipoAtributo> lista) {
		this.aumentoTipoAtributos = lista;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<AumentoTipoAtributo> getAumentoTipoAtributos() {
		return aumentoTipoAtributos;
	}

	public String getNombre() {
		return this.nombre;
	}

	public int getCantDePersonajes() {
		return this.cantDePersonajes;
	}
	public void setCantDePersonajes(int i) {
		this.cantDePersonajes = i;
	}

	public Integer getId() {
		return id;
	}
}
