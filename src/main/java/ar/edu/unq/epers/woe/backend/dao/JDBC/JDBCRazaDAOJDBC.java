package ar.edu.unq.epers.woe.backend.dao.JDBC;


import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.service.raza.NoSePuedeCrearOtraaRazaYaExistenteException;

import java.sql.*;
import java.util.*;


public class JDBCRazaDAOJDBC implements RazaDAOJDBC {

    @Override
    public void guardar(Raza raza) {
        JDBCConectionBlock.executeWithConnection(conn -> {
            PreparedStatement ps = conn.prepareStatement("INSERT INTO raza ( nombre, clases,peso,altura,energiaInicial,urlFoto,cantidadPersonajes) VALUES (?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            setInicial(ps,raza);


            //ojo, no estamos guardando el inventario!
            ps.execute();

            if (ps.getUpdateCount() != 1) {
                throw new NoSePuedeCrearOtraaRazaYaExistenteException(raza);
            }

            try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                if (generatedKeys.next()) {
                      raza.setId(this.convertirLongEnInteget(generatedKeys.getLong(1)));
                } else {
                     throw new SQLException("Creating user failed, no ID obtained.");
                 }
            }
            ps.close();

            return null;
        } );


    }

    private Integer convertirLongEnInteget(long aLong) {
        return (int)aLong;
    }

    private Integer obtenerUltimoID() {

        return JDBCConectionBlock.executeWithConnection(conn -> {
            PreparedStatement ps = conn.prepareStatement("Select MAX(id) from raza");
            ResultSet resultSet = ps.executeQuery();
            int ultimoID;

            ultimoID = resultSet.getInt(1);
            ps.close();
            return ultimoID;




        });

    }


   private  String convertirClasesEnString(Set<Clase> clases){
    return "";

      // return clases.stream().map( clase -> clase.name()).collect(Collectors.joining(","));

    }
 private Set<Clase> recuperarClasesEnString(String s){
       Set<Clase> setclase = new HashSet<>();
       Clase clase = new Clase("mago");
       setclase.add(clase);
     return setclase;
     //Arrays.stream(s.split(",")).map(clase -> Clase.valueOf(clase)).collect(Collectors.toSet());
 }
    @Override
    public Raza recuperar(Integer id) {
        return JDBCConectionBlock.executeWithConnection(conn -> {
            PreparedStatement ps = conn.prepareStatement("SELECT id, nombre,clases,peso,altura,energiaInicial,urlFoto,cantidadPersonajes FROM raza WHERE id = ?");
            ps.setInt(1, id);

            ResultSet resultSet = ps.executeQuery();

                Raza raza = null;
                while (resultSet.next()){
                    raza = this.datosIniciales(resultSet);

                }
                ps.close();
                return raza;
        } );
    }


    public Raza datosIniciales(ResultSet resultSet) throws SQLException {

        Raza raza = new Raza();

        raza.setId(resultSet.getInt("id"));
        raza.setNombre(resultSet.getString("nombre"));
        raza.setClases(this.recuperarClasesEnString(resultSet.getString("clases")));
        raza.setPeso(resultSet.getInt("peso"));
        raza.setAltura(resultSet.getInt("altura"));
        raza.setEnergiaIncial(resultSet.getInt("energiaInicial"));
        raza.setUrlFoto(resultSet.getString("urlFoto"));
        raza.setCantidadPersonajes(resultSet.getInt("cantidadPersonajes"));


        return raza;
    }

    public void eliminarTodosLosDatos() {

        JDBCConectionBlock.executeWithConnection(conn -> {
            PreparedStatement ps = conn.prepareStatement(
                    " TRUNCATE  TABLE raza");

            ps.executeUpdate();

            ps.close();

            return null;
        });

    }

    public List<Raza> recuperarTodos() {
        return JDBCConectionBlock.executeWithConnection(conn -> {
            PreparedStatement ps = conn.prepareStatement(
                    "SELECT id, nombre,clases,peso,altura,energiaInicial,urlFoto,cantidadPersonajes  " +
                            "FROM raza " +
                            "order by nombre ASC");


            ResultSet resultSet = ps.executeQuery();

            List<Raza> razas = new ArrayList<Raza>();

            while (resultSet.next()) {
                this.datosIniciales(resultSet);

                razas.add(datosIniciales(resultSet));
            }

            ps.close();
            return razas;
        });
    }

    public void actualizar(Raza raza) {
        JDBCConectionBlock.executeWithConnection(conn -> {
            PreparedStatement ps = conn.prepareStatement("update raza SET nombre = ?,clases = ?,peso = ?,altura = ?,energiaInicial = ?,urlFoto = ?, cantidadPersonajes = ? where id = ? ");

            setInicial(ps, raza);
            ps.setInt(8, raza.getId());

            ps.executeUpdate();
            ps.close();

            return raza;
        });
    }

    public void setInicial(PreparedStatement ps, Raza raza) throws SQLException {
        ps.setString(1,raza.getNombre());
        ps.setString(2,this.convertirClasesEnString(raza.getClases()));
        ps.setInt(3,raza.getPeso());
        ps.setInt(4,raza.getAltura());
        ps.setInt(5,raza.getEnergiaInicial());
        ps.setString(6,raza.getUrlFoto());
        ps.setInt(7,raza.getCantidadPersonajes());
    }
}
