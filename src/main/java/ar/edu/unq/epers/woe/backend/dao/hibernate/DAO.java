package ar.edu.unq.epers.woe.backend.dao.hibernate;

import java.io.Serializable;
import java.util.List;

public interface DAO<T> {

    void guardar(T  o);

    Object recuperar(Serializable id);

    List<T> recuperarTodos();

    void borrarTodo();
}
