package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class PreCondicionParaAceptacionMision {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;


    private  int nivel ;

    public PreCondicionParaAceptacionMision(){}

    public PreCondicionParaAceptacionMision(Integer nivel){
        this.nivel = nivel;
    }

    public Boolean cumpleConLaPreconDicion (Personaje personaje){
       return personaje.getNivel() == nivel;
    }

    public Integer getNivelPreciondicion() {
        return this.nivel;
    }

    public Integer getId(){return this.id;}
}
