package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;

@Entity
public class IrAUnLugar extends RequisitoMision {


    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Lugar lugarObjetivo;

    public IrAUnLugar(Lugar lugarObjetivo){
        this.lugarObjetivo = lugarObjetivo;
    }

    public IrAUnLugar(){}

    public Lugar getLugarRequisito(){
        return this.lugarObjetivo;
    }

    @Override
    public boolean cumplioRequisito(Personaje personaje) {
        return personaje.getLugar().equals(getLugarRequisito());
    }


    public void setLugarObjetivo(Lugar lugarObjetivo) {
        this.lugarObjetivo = lugarObjetivo;
    }

}
