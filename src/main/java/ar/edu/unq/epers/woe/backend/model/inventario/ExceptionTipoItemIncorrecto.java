package ar.edu.unq.epers.woe.backend.model.inventario;

public class ExceptionTipoItemIncorrecto extends RuntimeException {

    //TODO hay que poner annotations?

    public ExceptionTipoItemIncorrecto(Item item, TipoItem tipo){
        super("La ubicacion del Item " + item.getNombre() + " no coincide con la del tipo "+ tipo.name());
    }
}


