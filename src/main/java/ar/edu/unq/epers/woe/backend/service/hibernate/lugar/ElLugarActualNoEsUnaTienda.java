package ar.edu.unq.epers.woe.backend.service.hibernate.lugar;

public class ElLugarActualNoEsUnaTienda extends RuntimeException {
    public ElLugarActualNoEsUnaTienda() {
        super("El personaje no se encuentra en el gimnasio");
    }
}
