package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;

@Entity
public class GanarCombate extends RequisitoMision {

    private int cantAGanar; //cantidad de veces que tiene que ganar

    @ManyToOne(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    private Personaje personajeRival; //personaje contra el que teine que pelear

    public GanarCombate(int cantAGanar, Personaje personajeRival){
        this.cantAGanar = cantAGanar;
        this.personajeRival = personajeRival;
    }

    public GanarCombate(){}

    public Personaje getPersonajeRival(){
        return this.personajeRival;
    }

    public int getCantAGanar() {
        return cantAGanar;
    }

    @Override
    public boolean cumplioRequisito(Personaje personaje) {
        return personaje.getRegistroCombateContra(this.getPersonajeRival()).getCantGanadas() == this.getCantAGanar();
    }



    public void setCantAGanar(int cantAGanar) {
        this.cantAGanar = cantAGanar;
    }

    public void setPersonajeRival(Personaje personajeRival) {
        this.personajeRival = personajeRival;
    }
}
