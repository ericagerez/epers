package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.List;

@Entity
public class RecompensaItem extends Recompensa {



    @OneToMany
    private List<Item> items;

    public RecompensaItem(List<Item> items) {
        this.items = items;
    }

    public RecompensaItem(){}

    @Override
    public void darRecompensaCorrespondiente(Personaje personaje) {
        this.items.stream().forEach(item -> personaje.guarItemEnMochila(item));
    }

    public void setItems(List<Item> items) {
        this.items = items;
    }
}