package ar.edu.unq.epers.woe.backend.model.personaje;

import ar.edu.unq.epers.woe.backend.model.combate.ResultadoCombate;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;

import javax.persistence.*;

@Entity
public class RegistroCombate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.EAGER)
    private ResultadoCombate resultadoCombate;

    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.EAGER)
    private Rival rival;

    private int cantGanadas = 0;

    @OneToOne(cascade= CascadeType.ALL, fetch = FetchType.EAGER)
    private Personaje personaje; //el que pelea contra rival

    public RegistroCombate(ResultadoCombate resultadoCombate, Rival rival, Personaje personaje) {
        this.resultadoCombate = resultadoCombate;
        this.rival = rival;
        this.personaje = personaje;
    }

    public RegistroCombate(){}

    public Rival getRival(){
        return this.rival;
    }

    public int getCantGanadas(){
        return this.cantGanadas;
    }

    public void setResultadoCombate(ResultadoCombate resultadoCombate) {
        this.resultadoCombate = resultadoCombate;
    }

    public void setRival(Rival rival) {
        this.rival = rival;
    }

    public void setCantGanadas(int cantGanadas) {
        this.cantGanadas = cantGanadas;
    }

    public void setPersonaje(Personaje personaje) {
        this.personaje = personaje;
    }

    public void incrementarCantGanadasSiCorresponde(){
        if(this.resultadoCombate.getGanador().equals(this.personaje)){
            cantGanadas++;
        }
    }

}
