package ar.edu.unq.epers.woe.backend.service.hibernate.personaje;

import ar.edu.unq.epers.woe.backend.model.combate.ResultadoCombate;

public interface PersonajeService {

    void equipar(Integer personajeId, Integer itemId);

    ResultadoCombate combatir(Integer personajeID1, Integer personajeID2);

}


