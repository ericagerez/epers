package ar.edu.unq.epers.woe.backend.model.inventario;

public class ExceptionItemDiferenteUbicacionAlSlot extends RuntimeException{

    //TODO hay que poner annotations?

    public ExceptionItemDiferenteUbicacionAlSlot(Item item) {
        super("El item"+ item.getNombre()+" no tiene la misma ubicacion que el slot ");
    }
}
