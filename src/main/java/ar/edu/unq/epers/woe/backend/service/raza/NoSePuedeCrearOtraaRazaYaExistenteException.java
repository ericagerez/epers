package ar.edu.unq.epers.woe.backend.service.raza;

import ar.edu.unq.epers.woe.backend.model.raza.Raza;

public class NoSePuedeCrearOtraaRazaYaExistenteException extends RuntimeException {
    public NoSePuedeCrearOtraaRazaYaExistenteException(Raza unaraza){
        super("no se puede crear otra Raza con el mismo nombre" + unaraza.getNombre());

    }
}
