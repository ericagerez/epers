package ar.edu.unq.epers.woe.backend.model.requerimiento;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;

@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class Requisito {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    protected Integer valor;

    public Requisito(Integer valor) {
        this.valor = valor;
    }

    public Requisito(){}

     public abstract Boolean satisface(Personaje personaje);

     public Integer getValor(){
         return this.valor;
     }

    public void setValor(Integer valor) {
        this.valor = valor;
    }
}