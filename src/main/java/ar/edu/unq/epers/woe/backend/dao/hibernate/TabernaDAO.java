package ar.edu.unq.epers.woe.backend.dao.hibernate;


import ar.edu.unq.epers.woe.backend.model.lugar.Taberna;

public class TabernaDAO extends AbstractaDAO<Taberna> {
    public TabernaDAO(){super(Taberna.class);}

    @Override
    Class<Taberna> getClase() {
        return Taberna.class;
    }

}
