package ar.edu.unq.epers.woe.backend.model.inventario;

public class ExceptionUbicacionSlot extends RuntimeException {

    //TODO hay que poner annotations?

    public ExceptionUbicacionSlot(Slot slot){
        super("No es la ubicacion correcta:" + slot.getUbicacion());
    }
}
