package ar.edu.unq.epers.woe.backend.dao.hibernate;

import ar.edu.unq.epers.woe.backend.model.lugar.Gimnasio;

public class GimnasioDao extends AbstractaDAO<Gimnasio> {

    public GimnasioDao() {
        super(Gimnasio.class);
    }

    @Override
    Class getClase() {
        return Gimnasio.class;
    }



}
