package ar.edu.unq.epers.woe.backend.service.hibernate.lugar;

public class ElPersonajeNoEstaEnUnaTaberna extends RuntimeException {
    public ElPersonajeNoEstaEnUnaTaberna(){super("El personaje  no se encuentra en una Taberna");}
}
