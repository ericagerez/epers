package ar.edu.unq.epers.woe.backend.model.lugar;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Entity

public class Tienda extends Lugar{

    @OneToMany( cascade=CascadeType.ALL)
    private List<Item> itemsAVender;

    public Tienda(String nombreLugar) {
        super(nombreLugar);
        itemsAVender = new ArrayList<>();
    }

    public Tienda(){}

    public void venderItemAlPersonaje(Personaje personaje, Ubicacion ubicacion){
       try {if (this.buscarItemConubicacion(ubicacion) != null) {
           Item itemAvender = this.buscarItemConubicacion(ubicacion);
           personaje.comprarItem(itemAvender,itemAvender.getCostoDeVenta());
           this.sacarItem(itemAvender);
            }
       }catch (RuntimeException e ){
           throw new NoHayItemsAvenderDeEsaTipoDeUbicacion();
       }

    }

    public void sacarItem(Item itemAvender) {
        this.getItemsAvender().remove(itemAvender);
    }


    public void comprarItemAlPersonaje(Personaje personaje,Item item){
        //Precondicion: se confia que el item a vender el personaje lo posee en la mochila.
        this.agregarItem(item);
        personaje.venderItem(item,item.getCostoDeCompra());

    }

    public void agregarItem(Item item) {
        this.getItemsAvender().add(item);
    }

    public List<Item> getItemsAvender() {
        return this.itemsAVender;

    }


   public Item buscarItemConubicacion(Ubicacion ubicacion){
       return this.itemsAVender.stream()
               .filter(a -> a.getUbicacion() == ubicacion)
               .collect(toList()).get(0);

   }

    public boolean estaVacia() {
        return this.getItemsAvender().isEmpty();
    }

    public void setItemsAVender(ArrayList<Item> itemsAVender) {
        this.itemsAVender = itemsAVender;
    }


    @Override
    public boolean esTienda() {
        return true;
    }

    @Override
    public List<Item> getItems(){
        return this.itemsAVender;
    }


}
