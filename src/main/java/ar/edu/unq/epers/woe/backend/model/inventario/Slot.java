package ar.edu.unq.epers.woe.backend.model.inventario;

import javax.persistence.*;

@Entity
public class Slot {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;


    @Enumerated(EnumType.STRING)
    private Ubicacion ubicacion;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Item item;

    public Slot(Ubicacion ubicacion){
        this.setUbicacion(ubicacion);
    }

    public Slot(){}

    /****************************** GETTERS ******************************/

    public Item getItem(){return this.item;}
    public Ubicacion getUbicacion(){
        return this.ubicacion;
    }

    /****************************** SETTERS ******************************/

    public void setUbicacion( Ubicacion ubicacion){this.ubicacion = ubicacion;}
    public void setItem(Item item) { this.item = item; }

    /****************************** OTRAS ACCIONES ******************************/

    public void agregarItem(Item item) {
        try {
            if (item.getUbicacion() == this.ubicacion) {
                this.setItem(item);
            }
        }catch (RuntimeException e){
             throw new ExceptionItemDiferenteUbicacionAlSlot(item);
            }
    }

    public boolean tieneItem(){
        return this.getItem() != null;
    }


    public boolean puedeAgregarAItem(Item item) {
        return item.getUbicacion() == this.getUbicacion();
    }

    public Integer getId() {
        return id;
    }
}
