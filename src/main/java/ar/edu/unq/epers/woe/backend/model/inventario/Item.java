package ar.edu.unq.epers.woe.backend.model.inventario;

import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.lugar.Tienda;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.requerimiento.Requerimiento;
import ar.edu.unq.epers.woe.backend.model.requerimiento.Requisito;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

import static java.util.stream.Collectors.toList;

@Entity
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String nombre;

    @Enumerated(EnumType.STRING)
    private Ubicacion ubicacion;

    @Enumerated(EnumType.STRING)
    private TipoItem tipo;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Clase> clasesPermitidas; //clases Permitidas

    @OneToMany(cascade = CascadeType.ALL)
    private List<Requisito> requisitos;

    private Integer costoDeCompra;

    private Integer costoDeVenta;

    @OneToMany(cascade = CascadeType.ALL)
    private List<Atributo> atributos; //puede incrementarse cuando se consigue una recompensa

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "tienda_id")
    private Tienda tienda;


    public Item(String nombre, Ubicacion ubicacion, TipoItem tipo, Integer costoDeCompra, Integer costoDeVenta){
        this.setNombre(nombre);
        this.setUbicacion(ubicacion);
        this.setTipo(tipo);
        this.setCostoDeCompra(costoDeCompra);
        this.setCostoDeVenta(costoDeVenta);
        this.clasesPermitidas = new ArrayList<>();
        this.atributos = new ArrayList<>();
        this.requisitos = new ArrayList<>();
        this.atributos = new ArrayList<>();
    }

    public Item(){}

    /****************************** GETTERS ******************************/

    public Ubicacion getUbicacion(){
        return this.ubicacion;
    }
    public String getNombre(){
        return this.nombre;
    }
    public TipoItem getTipo() { return this.tipo; }
    public Integer getCostoDeCompra(){return this.costoDeCompra; }
    public Integer getCostoDeVenta(){return this.costoDeVenta;}
    public Integer getId() { return id; }

    public List<Atributo> getAtributos(){return this.atributos;}
    public List<Requisito> getRequisitos(){ return this.requisitos;}
    public List<Clase> getClasesPermitidas(){return this.clasesPermitidas;}


    /****************************** SETTERS ******************************/

    public void setNombre(String nombre){ this.nombre = nombre;}
    public void setUbicacion(Ubicacion ubicacion){this.ubicacion = ubicacion;}

    public void setTipo(TipoItem tipo){
        try {
            if (tipo.tieneUbicacionEn(this.ubicacion)) {
                this.tipo = tipo;
            }
        }
        catch (Exception e){
            throw new ExceptionTipoItemIncorrecto(this, tipo);
        }

    }

    public void setCostoDeCompra(Integer costoC){this.costoDeCompra = costoC;}
    public void setCostoDeVenta(Integer costoV){this.costoDeVenta = costoV;}
    public void setRequisitos(ArrayList<Requisito> requisitos){this.requisitos = requisitos;}
    public void setAtributos(ArrayList<Atributo> atributos){this.atributos = atributos;}
    public void setClasesPermitidas(ArrayList<Clase> clases){this.clasesPermitidas = clases;}

    /****************************** GETTERS ******************************/

    public Atributo getAtributo(TipoAtributo tipoAtributo) {
        return (Atributo) this.atributos.stream()
                .filter(a -> a.getTipo() == tipoAtributo)
                .collect(toList())
                 .get(0);
    }

   public double getDannoDelAtributo(){
     return  this.getAtributo(TipoAtributo.DANNO).getValor();
   }

    /****************************** OTRAS ACCIONES ******************************/

    public void addClase(Clase clase){
        this.getClasesPermitidas().add(clase);
    }
    public void addRequerimiento(Requisito r){
        this.requisitos.add(r);
    }
    public void addAtributo(Atributo a){
        this.atributos.add(a);
    }
    public boolean tieneLaClase(Clase clase) {
        return this.getClasesPermitidas().contains(clase);
    }
    public boolean tieneElRequerimiento(Requisito requisito) {
        return this.getRequisitos().contains(requisito);
    }
    public boolean tieneElAtributo(Atributo atributo) {
        return  this.getAtributos().contains(atributo);
    }

    public boolean cumploConTodosLosRequisitos(Personaje personaje){
        Requerimiento r = new Requerimiento(requisitos);
        return r.satisface(personaje);
    }

}
