package ar.edu.unq.epers.woe.backend.model.lugar;

import ar.edu.unq.epers.woe.backend.model.misiones.Mision;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Taberna extends Lugar {

    @OrderColumn(name="orderIndex")
    @OneToMany(cascade=CascadeType.ALL)
    public List<Mision> misiones;


    public Taberna(String nombreLugar) {
        super(nombreLugar);
        misiones = new ArrayList<Mision>();
    }

    public Taberna(){}

    @Override
    public boolean esTaberna() {
        return true;
    }

    public List<Mision> getMisiones(){
        return this.misiones;
    }


    public void aceptarMision(Personaje personaje,Mision mision){
        //Prec.: mision debe pertenecer a la lista misiones.  se fija si esta en listar mision en las que puedo aceptar.

            if(this.listarMisiones(personaje).contains(mision)){
            // pregunta si pueda hacerla  lo tiene que hacer el personaje
            personaje.aceptarMision(mision);  // requerimiento
             }

    }


    public List<Mision> listarMisiones(Personaje personaje){
        if(personaje.getMisionesAceptadas().isEmpty()){
            return  this.getMisiones();
        }

        else{
            return this.renovarMisiones(personaje,this.getMisiones());}
    }

    public void setMisiones(ArrayList<Mision> misiones){
        this.misiones = misiones;
    }

    public void addMision(Mision mision){
        this.misiones.add(mision);}

    public List<Mision> renovarMisiones(Personaje personaje, List<Mision> misiones){

        List<Mision>mreturn = new ArrayList<Mision>();

        for (Mision m : misiones){

            if(personaje.getMisionesCompletadas().contains(m) || personaje.getMisionesAceptadas().contains(m)){

                mreturn.addAll( this.renovarMisiones(personaje,m.getMisionesHijas()));


            } else{
                 mreturn.add(m);
                }

        }
            return mreturn;
    }
}


