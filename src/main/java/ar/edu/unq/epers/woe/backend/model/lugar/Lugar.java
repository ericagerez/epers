package ar.edu.unq.epers.woe.backend.model.lugar;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.List;


@Entity
@Inheritance(strategy = InheritanceType.JOINED)

public abstract class Lugar {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    private String nombreLugar;


    public Lugar(String nombreLugar) {
        this.nombreLugar = nombreLugar;
    }

    public Lugar(){}

    public String getNombreLugar(){return this.nombreLugar;}
    public String getNombre(){
        return this.nombreLugar;
    }

    public void setNombreLugar(String nombreLugar){this.nombreLugar = nombreLugar;}

    public Integer getId(){return this.id;}

    public boolean esTaberna() {
        return false;
    }

    public boolean esGimnasio(){return false;}

    public boolean esTienda(){return false;}

    public List<Item> getItems(){return null;}

}
