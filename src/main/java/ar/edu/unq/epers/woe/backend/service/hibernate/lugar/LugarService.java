package ar.edu.unq.epers.woe.backend.service.hibernate.lugar;

import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.misiones.Mision;

import java.util.List;

interface LugarService {

     List<Mision> listarMisiones(Integer idPersonaje);

    void aceptarMision(Integer idPersonaje, Integer idMisión);

    void completarMision(Integer idPersonaje, Integer idMisión);

    void mover(Integer idPersonaje, Integer idLugar);

    List<Item> listarItems(Integer idPersonaje);

    void comprarItem(Integer idPersonaje, Integer idItem);

    void venderItem(Integer idPersonaje, Integer idItem);
}
