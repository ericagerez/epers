package ar.edu.unq.epers.woe.backend.service.data;

import ar.edu.unq.epers.woe.backend.dao.JDBC.JDBCRazaDAOJDBC;
import ar.edu.unq.epers.woe.backend.dao.Neo4j.LugarDaoNeo4j.LugarDaoNeo4j;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class JDBCDataServiceImplementation implements JDBCDataService {

    private JDBCRazaDAOJDBC razaDAO = new JDBCRazaDAOJDBC();


    @Override
    public void eliminarDatos(){
      razaDAO.eliminarTodosLosDatos();
    }

    @Override
    public void crearSetDatosIniciales() {

        datosIniciales().forEach(raza -> {
            razaDAO.guardar(raza);
        });
    }

    private List<Raza> datosIniciales(){

        List<Raza> razas = new ArrayList<Raza>();
        Clase mago = new Clase("mago");

        Raza raza;
        Set<Clase> clases = new HashSet<Clase>();
        clases.add(mago);

        raza = new Raza();

        raza.setNombre("Hipogrifo");
        raza.setAltura(10);
        raza.setPeso(50);
        raza.setClases(clases);
        raza.setUrlFoto("");
        raza.setCantidadPersonajes(0);
        raza.setEnergiaIncial(100);
        //raza.setId(1);

        razas.add(raza);


        return razas;
    }
    public void  eliminarDatosNeo4j(){
        LugarDaoNeo4j lugarDaoNeo4j = new LugarDaoNeo4j();
        lugarDaoNeo4j.tearDown();

    }

}
