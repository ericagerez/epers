package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;


@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public abstract class RequisitoMision {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;
    //esta clase simplemente funciona como una interface,
    // para que la clase Mision reconozca de la misma manera a
    // todos los requisitos

    public boolean cumplioRequisito(Personaje personaje){return false;}

    public RequisitoMision(){}


    public Integer getId(){return this.id;}

}