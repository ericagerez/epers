package ar.edu.unq.epers.woe.backend.model.atributo;

import javax.persistence.*;
import javax.persistence.criteria.CriteriaBuilder;

@Entity
public class Atributo {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoAtributo tipo;

    private double valor;


    public Atributo(TipoAtributo tipo, double valor){
        this.setValor(valor);
        this.setTipo(tipo);

    }

    public Atributo(){}

    public TipoAtributo getTipo(){
        return this.tipo;
    }

    public double getValor() {
        return this.valor;
    }

    public void setValor(double nuevoValor) {
        this.valor = nuevoValor;
    }

    public void setTipo(TipoAtributo tipo) { this.tipo = tipo; }

    public Integer getId(){return id;}
}

