package ar.edu.unq.epers.woe.backend.model.combate;


import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class ResultadoCombate {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Rival ganador;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Rival perdedor;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Personaje> campeones = new ArrayList<>();

    public ResultadoCombate() {
    }

    /****************************** GETTERS ******************************/

    public Rival getGanador() {
        return ganador;
    }
    public Rival getPerdedor() {
        return perdedor;
    }
    public List<Personaje> getCampeones() {
        return campeones;
    }
    public Integer getId(){return id;}

    /****************************** SETTERS ******************************/

    public void setGanador(Rival ganador) {
        this.ganador = ganador;
    }
    public void setPerdedor(Rival perdedor) {
        this.perdedor = perdedor;
    }
    public void setCampeones(ArrayList<Personaje> campeones) {
        this.campeones = campeones;
    }


    /****************************** OTRAS ACCIONES ******************************/

    public boolean noHayGanador(Rival personaje, Rival rival,double vidaPersonaje,double vidaRival){
        return vidaPersonaje>0  && vidaRival>0 ;
    }
    public void agregarCampeon(Rival rival) {
        getCampeones().add((Personaje) rival);
    }

   public void ganadorDeCombate(Rival personaje,Rival rival,double vidaPersonaje,double vidaRival){
       if (rival.esPersonaje() && vidaRival > 0){
           this.setGanador(rival);
           this.agregarCampeon(rival);
           this.setPerdedor(personaje);
           rival.setCantPuntosDeExperiencia(rival.getCantPuntosDeExperiencia()+ 10*rival.getNivel());
       }else{
           this.setGanador(personaje);
           this.agregarCampeon(personaje);
           this.setPerdedor(rival);
           personaje.setCantPuntosDeExperiencia(personaje.getCantPuntosDeExperiencia()+ 10*personaje.getNivel());
       }
   }

    public double getAtaquePersonaje(Rival personaje, Rival rival) {
       return personaje.atacarAlOponente(rival);
    }

    public double getAtaqueRival(Rival rival, Rival personaje) {
        return rival.atacarAlOponente(personaje);
    }

}

