package ar.edu.unq.epers.woe.backend.service.hibernate.lugar;

import ar.edu.unq.epers.woe.backend.dao.Neo4j.LugarDaoNeo4j.LugarDaoNeo4j;
import ar.edu.unq.epers.woe.backend.dao.hibernate.*;
import ar.edu.unq.epers.woe.backend.dao.mongo.EventoDAO;
import ar.edu.unq.epers.woe.backend.model.eventos.*;
import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.lugar.Gimnasio;
import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.lugar.Taberna;
import ar.edu.unq.epers.woe.backend.model.misiones.Mision;
import ar.edu.unq.epers.woe.backend.model.misiones.Recompensa;
import ar.edu.unq.epers.woe.backend.model.misiones.RequisitoMision;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.service.Neo4J.CriterioMovimiento;
import ar.edu.unq.epers.woe.backend.service.Neo4J.TramoMuyCostoso;
import ar.edu.unq.epers.woe.backend.service.Neo4J.UbicacionMuyLejana;
import ar.edu.unq.epers.woe.backend.service.hibernate.personaje.PersonajeImplService;
import ar.edu.unq.epers.woe.backend.service.hibernate.personaje.PersonajeService;
import ar.edu.unq.epers.woe.backend.service.mongo.FeedServiceImpl;

import java.util.ArrayList;
import java.util.List;

import static ar.edu.unq.epers.woe.backend.service.hibernate.runner.TransactionRunner.run;

public class LugarImplService implements LugarService {
    private PersonajeDAO personajeDAO;
     private MisionDAO misionDAO;
     private LugarDAO lugarDAO;
     private ItemDAO itemDAO;
     private TabernaDAO tabernaDAO;
     private GimnasioDao gimnasioDao;
     private FeedServiceImpl feedService;
     private LugarDaoNeo4j lugarDAONOQSL;
     private EventoDAO emd;
     private PersonajeImplService personajeService;



     public LugarImplService(LugarDAO lugarDAO, PersonajeDAO personajeDAO, MisionDAO misionDAO, ItemDAO itemDAO, TabernaDAO tabernaDAO, GimnasioDao gimnasioDao, LugarDaoNeo4j lugarDaoNeo4j, EventoDAO eventoDAO) {
          this.lugarDAO = lugarDAO;
          this.personajeDAO = personajeDAO;
          this.misionDAO = misionDAO;
          this.itemDAO = itemDAO;
          this.tabernaDAO = tabernaDAO;
          this.gimnasioDao = gimnasioDao;
          this.lugarDAONOQSL = lugarDaoNeo4j;
          this.emd = eventoDAO;
          this.personajeService = new PersonajeImplService(personajeDAO,itemDAO,new ClaseDataDAO(),gimnasioDao,new ResultadoCombateDao(), new EventoDAO());
     }

     public void setFeedService(FeedServiceImpl feedService){
          this.feedService = feedService;
     }

     public void guardarPersonaje(Personaje personaje) {
          run(() -> {
               this.personajeDAO.guardar(personaje);
          });
     }

     public Personaje recuperarPersonaje(Integer personajeId) {
          return run(() -> this.personajeDAO.recuperar(personajeId));
     }



     public void guardarTaberna(Taberna taberna) {
          run(() -> {
               this.tabernaDAO.guardar(taberna);
          });
     }

     public Taberna recuperarTaberna(Integer tabernaId) {
          return run(() -> this.tabernaDAO.recuperar(tabernaId));
     }

     @Override
     public List<Mision> listarMisiones(Integer idPersonaje) {
     /**
      * Devuelve la lista de misiones disponibles para un jugador.
      * Validar que el personaje se encuentre en una Taberna**/
         return run(() -> {
              Personaje personaje = personajeDAO.recuperar(idPersonaje);

              if (personaje.getLugar().esTaberna()){
                   Taberna taberna = (Taberna) personaje.getLugar();

                   return taberna.listarMisiones(personaje);


              }else {throw new ElPersonajeNoEstaEnUnaTaberna();}

              });
     }

     @Override
     public void aceptarMision(Integer idPersonaje, Integer idMisión) {
     /**
      * El personaje acepta la mision. Validar que el personaje
      * se encuentre en una Taberna y que la mision este disponible.**/
          run(() -> {

               Personaje personaje = personajeDAO.recuperar(idPersonaje);
               Mision mision = misionDAO.recuperar(idMisión);
          if(!personaje.getLugar().getClass().equals(Taberna.class)){
               throw  new ElPersonajeNoEstaEnUnaTaberna();

          }else if (this.listarMisiones(personaje.getId()).contains(mision)) {
              this.emd.save(new MisionAceptada(personaje.getNombre(),personaje.getLugar().getNombre(),mision.getNombre()));
              personaje.aceptarMision(mision);

          }



               return null;
          });
     }

     @Override
     public void completarMision(Integer idPersonaje, Integer idMisión) {
     /**
      * El personaje completa la misión (si la habia aceptado previamente y cumple todos los requisitos)
      *  y recibe la recompensa. Validar que la misión esté disponible y que pueda recibir la recompensa
      *  (por ejemplo, si la recompensa es un item y tiene la mochila llena, no puede recibir la misma)**/
          Mision mision = misionDAO.recuperar(idMisión);
          Personaje personaje = personajeDAO.recuperar(idPersonaje);
          RequisitoMision requisitoMision = mision.getRequisito();
          Recompensa recompensa = mision.getRecompensa();

               if(personaje.getMisionesAceptadas().contains(mision)){
                    mision.update(personaje);
               }

          //TODO este evento se debe levantar cuando se asegure que el personaje haya completado la mision

     }


     @Override
     public List<Item> listarItems(Integer idPersonaje) {
     /**
      * Devuelve una lista de items disponibles que tiene la tienda. Validar que el personaje se encuentre en una Tienda.**/

          return run(() -> {
               List<Item> items;
               Personaje personaje = personajeDAO.recuperar(idPersonaje);

               if (personaje.getLugar().esTienda()) {
                    items = personaje.getLugar().getItems();
                    return items;
               }
               else{
                     throw new ElLugarActualNoEsUnaTienda();
               }

          });
     }

     @Override
     public void comprarItem(Integer idPersonaje, Integer idItem) {
     /**
      * El personaje obtiene el item y se le debita el costo del mismo.
      * Validar que tenga la cantidad de monedas necesarias para comprar el item. **/
          run(() -> {
                  Personaje personaje = personajeDAO.recuperar(idPersonaje);
                  Item itemAComprar = itemDAO.recuperar(idItem);
                 try {
                      if (personaje.getLugar().esTienda()) {
                          List<String> mis = this.personajeService.misionesCumplidasPor(personaje);
                          personaje.comprarItem(itemAComprar, itemAComprar.getCostoDeVenta());
                           this.emd.save(new ComprarItem(personaje.getNombre(), personaje.getLugar().getNombre(), itemAComprar.getNombre(), itemAComprar.getCostoDeCompra()));
                          this.generarEventoMisCompSiCorresponde(mis, personaje);
                      }
                 } catch(RuntimeException e) {
                      throw new RuntimeException("El Personaje no está en una Tienda.");

                 }
                 });


     }

     @Override
     public void venderItem(Integer idPersonaje, Integer idItem) {
     /**
          *El personaje vende el item y se le acredita el costo del mismo.**/
          run(() -> {
               Item item = itemDAO.recuperar(idItem);
               Personaje personaje = personajeDAO.recuperar(idPersonaje);
               try {
                    if(personaje.getLugar().esTienda()) {
                         personaje.venderItem(item, item.getCostoDeCompra());

                         this.emd.save(new VenderItem(personaje.getNombre(), personaje.getLugar().getNombre(), item.getNombre(), item.getCostoDeVenta()));

                    }
               }catch (RuntimeException e){
                    throw new RuntimeException("El Personaje no está en una Tienda.");

               }
          });
     }

     public void guardarGimnasio(Gimnasio gimnasio) {
          run(() -> {
               this.gimnasioDao.guardar(gimnasio);
          });
     }


     public Gimnasio recuperarGimnasio(Integer gimnasioId) {
          return run(() -> this.gimnasioDao.recuperar(gimnasioId));
     }

     public void guardarMisionEnLugar(Mision misionIrAUnLugar) {
          run(()->{this.misionDAO.guardar(misionIrAUnLugar);});
     }

     public Mision recuperarMisionEnLugar(Integer id){
          return run(() ->misionDAO.recuperar(id));
     }

     public void crearUbicacion(Lugar l) {
          run(() -> {
               this.lugarDAO.guardar(l);
               this.lugarDAONOQSL.create(l);
          });
     }

     @Override
     public void mover(Integer idPersonaje, Integer idLugar) {
          /**
           * Cambia la ubicación actual del personaje por la especificada por parámetro.**/
          run(() -> {

               Personaje personaje = personajeDAO.recuperar(idPersonaje);
               Lugar lugaraMover = lugarDAO.recuperar(idLugar);
               List<String> mis = this.personajeService.misionesCumplidasPor(personaje);

               validarRequisitosParaMover(personaje, lugaraMover, CriterioMovimiento.masBarato.ordinal());
              this.emd.save(new Arribo(personaje.getNombre(), personaje.getLugar().getNombre(), personaje.getLugar().getClass().getSimpleName(),
                      lugaraMover.getNombre(), lugaraMover.getClass().getSimpleName()));

               personaje.disminuirDeLaBilletera(this.lugarDAONOQSL.costoDelTrayectoMasBarato(personaje.getLugar().getNombre(),lugaraMover.getNombre()));
               personaje.setLugar(lugaraMover);
               this.generarEventoMisCompSiCorresponde(mis, personaje);

          });
     }

     private void generarEventoMisCompSiCorresponde(List<String> mis, Personaje personaje) {
          List<String> misComp = personajeService.misionesCumplidasPor(personaje);
          if(personaje.getNombre().equals(mis.get(0)) && personaje.getMisionesCompletadas().size() > (mis.size()-1)) {
               misComp.removeAll(mis);
               this.emd.save(new MisionCompletada(personaje.getNombre(), personaje.getLugar().getNombre(), misComp.iterator().next()));
          }
     }

     public void validarRequisitosParaMover(Personaje personaje, Lugar lugar, int criterio) {
          if(!this.lugarDAONOQSL.existeRuta(personaje.getLugar().getNombre(),lugar.getNombre())) {
               throw new UbicacionMuyLejana();
          }
          Integer costoViaje = null;
          switch(criterio) {
               case 0: costoViaje = this.lugarDAONOQSL.costoDelTrayectoMasBarato(personaje.getLugar().getNombre(), lugar.getNombre()); break;
               case 1: costoViaje = this.lugarDAONOQSL.costoDelTrayectoMasBarato(personaje.getLugar().getNombre(), lugar.getNombre()); break;
          }
          if(costoViaje > personaje.getBilletera()) {
               throw new TramoMuyCostoso();
          }
     }

     public void moverMasCorto(Integer idPersonaje,Integer idLugar){
          run(() -> {
               Personaje personaje = personajeDAO.recuperar(idPersonaje);
               Lugar lugaraMover = lugarDAO.recuperar(idLugar);
               List<String> mis = this.personajeService.misionesCumplidasPor(personaje);
               validarRequisitosParaMover(personaje, lugaraMover, CriterioMovimiento.masCorto.ordinal());
              this.emd.save(new Arribo(personaje.getNombre(), personaje.getLugar().getNombre(), personaje.getLugar().getClass().getSimpleName(),
                      lugaraMover.getNombre(), lugaraMover.getClass().getSimpleName()));
              personaje.disminuirDeLaBilletera(lugarDAONOQSL.costoTrayectoMasCorto(personaje.getLugar().getNombre(),lugaraMover.getNombre()));
               personaje.setLugar(lugaraMover);
               this.generarEventoMisCompSiCorresponde(mis, personaje);


          });
     }
     public void conectar(String nombreL1, String nombreL2, String tipoCamino) {
          run(() -> {

               lugarDAONOQSL.crearRuta(nombreL1,nombreL2,tipoCamino);
          });
     }
     public List<Lugar> conectados(Integer idLugar, String tipoCamino) {

          List<Lugar> res = new ArrayList<Lugar>();
          run(()-> {
               Lugar lugar = lugarDAO.recuperar(idLugar);

               for (String nombreLugar : lugarDAONOQSL.conectados(lugar.getNombre(), tipoCamino)) {
                    res.add(lugarDAO.recuperarPorNombre(nombreLugar));
               }
          });
          return res;

     }


}



