package ar.edu.unq.epers.woe.backend.model.misiones;

import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;

import javax.persistence.*;
import java.util.List;

@Entity
public class RecompensaMision extends Recompensa {

    @OneToMany
    List<Mision> misionesNuevas;

    public RecompensaMision(List<Mision> misionesNuevas) {
        this.misionesNuevas = misionesNuevas;
    }

    public RecompensaMision(){}

    @Override
    public void darRecompensaCorrespondiente(Personaje personaje) {
        }

    public void setMisionesNuevas(List<Mision> misionesNuevas) {
        this.misionesNuevas = misionesNuevas;
    }
}
