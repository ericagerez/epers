package ar.edu.unq.epers.woe.backend.model.monstruo;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

public enum TipoMonstruo {
    DRAGON,
    LOBO,
    TIGRE,
    DUENDE;

    @Id
    @GeneratedValue
    int id;

     TipoMonstruo(){}

}
