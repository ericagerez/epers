package ar.edu.unq.epers.woe.backend.model.combate;
import ar.edu.unq.epers.woe.backend.model.personaje.Personaje;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;

import javax.persistence.*;

@Entity
public class Combate {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Personaje personaje;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Rival rival;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private ResultadoCombate resultadoCombate;

    public Combate(Personaje personaje, Rival rival){
        this.setPersonaje(personaje);
        this.setRival(rival);
        this.resultadoCombate = new ResultadoCombate();
    }

    public Combate(){}

    /****************************** GETTERS ******************************/

    public Rival getPersonaje() {
        return personaje;
    }
    public Rival getRival() {
        return rival;
    }
    public ResultadoCombate getResultadoCombate(){return resultadoCombate;}

    /****************************** SETTERS ******************************/

    public void setRival(Rival rival) {
        this.rival = rival;
    }
    public void setPersonaje(Personaje personaje) {
        this.personaje = personaje;
    }
    public void setResultadoCombate(ResultadoCombate resultadoCombate){this.resultadoCombate = resultadoCombate;}

    /****************************** OTRAS ACCIONES ******************************/

    public ResultadoCombate realizarCombate(Rival personaje,Rival rival) {

                double vidaRival = rival.getVida();
                double vidaPersonaje = personaje.getVida();

                while (resultadoCombate.noHayGanador(personaje, rival, vidaPersonaje, vidaRival)) {
                    double vidaARestarAlRival = personaje.atacarAlOponente(rival);
                    vidaRival -= vidaARestarAlRival;
                    if (resultadoCombate.noHayGanador(personaje, rival, vidaPersonaje, vidaRival)) {
                        double vidaARestarAlPersonaje = rival.atacarAlOponente(personaje);
                        vidaPersonaje -= vidaARestarAlPersonaje;
                    }
                }
                resultadoCombate.ganadorDeCombate(personaje, rival, vidaPersonaje, vidaRival);
                this.resultadoCombate.ganadorDeCombate(personaje, rival, vidaPersonaje, vidaRival);
                return this.resultadoCombate;
            }

    }
