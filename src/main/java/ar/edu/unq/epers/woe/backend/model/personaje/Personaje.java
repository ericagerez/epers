package ar.edu.unq.epers.woe.backend.model.personaje;

import ar.edu.unq.epers.woe.backend.model.atributo.TipoAtributo;
import ar.edu.unq.epers.woe.backend.model.combate.Combate;
import ar.edu.unq.epers.woe.backend.model.combate.ResultadoCombate;
import ar.edu.unq.epers.woe.backend.model.inventario.Inventario;
import ar.edu.unq.epers.woe.backend.model.inventario.Item;
import ar.edu.unq.epers.woe.backend.model.inventario.Ubicacion;
import ar.edu.unq.epers.woe.backend.model.lugar.Lugar;
import ar.edu.unq.epers.woe.backend.model.raza.Clase;
import ar.edu.unq.epers.woe.backend.model.raza.Raza;
import ar.edu.unq.epers.woe.backend.model.atributo.Atributo;
import ar.edu.unq.epers.woe.backend.model.rival.Rival;
import ar.edu.unq.epers.woe.backend.model.misiones.Mision;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.*;

import static java.util.stream.Collectors.*;

/**
 * Un {@link Personaje} existente en el sistema, el mismo tiene un nombre
 * y pertenece a una {@link Raza} en particular.
 *
 * @author Charly Backend
 */

@Entity
public class Personaje extends Rival {


	private String nombre;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Raza raza;

	@OrderColumn(name="orderIndex")
	@ManyToMany(cascade=CascadeType.ALL)
	private List<Atributo> atributos= new ArrayList<>();

	private int cantPuntosDeExperiencia;
	private int expAnterior;
	private int billetera;
	private int nivel;
	private int energia;
	private int combatesGanados;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Inventario inventario;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Clase clase;

	@ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private Lugar lugar;

	@ManyToMany(cascade=CascadeType.ALL)
	private List<Item> mochila = new ArrayList<>();


	@ManyToMany( cascade=CascadeType.ALL) //Muchos personajes pueden tenes muchas misiones aceptadas
	private List<Mision> misionesAceptadas = new ArrayList<>();
	/** Esta es la observerCollection del Observer**/

	@ManyToMany(cascade=CascadeType.ALL)
	private List<RegistroCombate> historialCombates = new ArrayList<>();


	//@ManyToOne (cascade = CascadeType.ALL, targetEntity = Mision.class)
	@ManyToMany( cascade=CascadeType.ALL) //Muchos personajes pueden tenes muchas misiones aceptadas
	private List<Mision> misionesCompletadas= new ArrayList<>();

	public Personaje(Raza raza, String nombre, Clase clase) {
		super();
		this.setRaza(raza);
		this.setNombre(nombre);
		this.setClase(clase);
		this.setCantPuntosDeExperiencia(100);
		this.setNivel(1);
		this.setBilletera(0);
		this.setEnergia(100);
		this.setExperienciaAnterior(this.getCantPuntosDeExperiencia());

	}

	public Personaje(){}

	/****************************** GETTERS ******************************/


	public Inventario getInventario(){ return this.inventario;}

	public int getExpAnterior(){return this.expAnterior;}

	public String getNombre() { return this.nombre; }



	public Raza getRaza() {
		return this.raza;
	}

	public int getEnergia() {
		return this.energia;
	}

	public Clase getClase() {
		return clase;
	}

	public List<Item> getMochila() { return mochila; }

	public int getBilletera() {
		return billetera;
	}

	public int getNivel() { return nivel; }

	public int getCantPuntosDeExperiencia() {
		return cantPuntosDeExperiencia;
	}

	public List<Atributo> getAtributos(){ return this.atributos;}

	public Atributo getAtributo(TipoAtributo tipoAtributo) {
		return this.atributos.stream()
				.filter(a -> a.getTipo() == tipoAtributo)
				.collect(toList()).get(0);
	}
	@Override
	public Lugar getLugar(){ return this.lugar;}

	@Override
	public void setCombatesGanados(int i) {
		this.combatesGanados = i;
	}


	public List<Mision> getMisionesAceptadas(){
		return this.misionesAceptadas;
	}


	public List<Mision>getMisionesCompletadas(){return this.misionesCompletadas;}

	/****************************** SETTERS ******************************/


	public void setInventario(Inventario inventario){ this.inventario = inventario;}

	public void setExperienciaAnterior(int cantPuntosDeExperiencia) {
		this.expAnterior = cantPuntosDeExperiencia;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public void setRaza(Raza raza) {
		this.raza = raza;
	}

	public void setEnergia(int energia) {
		this.energia = energia;
	}

	public void setClase(Clase clase) {
		this.clase = clase;
	}

	public void setMochila(ArrayList<Item> mochila) {this.mochila = mochila;}

	public void setBilletera(int billetera) {
		this.billetera = billetera;
	}

	public void setNivel(int nivel) {
		this.nivel = nivel;
	}

	public void setAtributos(ArrayList<Atributo> atributos){this.atributos = atributos;}

	public void setCantPuntosDeExperiencia(int cantPuntosDeExperiencia) {
		this.cantPuntosDeExperiencia = cantPuntosDeExperiencia;
	}

	public void setLugar(Lugar lugar){ this.lugar = lugar; }


	public void setExpAnterior(int expAnterior) {
		this.expAnterior = expAnterior;
	}

	public void setMisionesAceptadas(List<Mision> misionesAceptadas) {
		this.misionesAceptadas = misionesAceptadas;
	}


	public void setHistorialCombates(ArrayList<RegistroCombate> historialCombates) {
		this.historialCombates = historialCombates;
	}

	public void setMisionesCompletadas(List<Mision> misiones){
		this.misionesCompletadas= misiones;
	}

	/****************************** OTRAS ACCIONES DE PERSONAJE ******************************/

	//TODO falta lo que deberia hacer si no puede equipar el item
	public void equiparItemAlInventario(Item item) {

			if(item.cumploConTodosLosRequisitos(this)){
				this.getInventario().equipar(item, this);
				this.notificarMisiones(); /** Se notifica a los objetos observadores **/
			}
	}

    public void guarItemEnMochila(Item unItem){
    //TODO chequear que los items cumplan los requisitos necesarios para que el personaje los incorpore
		try{
		    if(this.mochila.size()<10 ){
				this.mochila.add(unItem);
				this.notificarMisiones(); /** Se notifica a los objetos observadores **/
		}}catch (RuntimeException e){
			throw  new ExceptionMochila();
		}
	}

	public boolean contieneElItemLaMochila(Item item){ return this.mochila.contains(item); }

	public void ganaPuntosDeExperiencia(int puntosDeExperiencia) {
		this.setExperienciaAnterior(this.getCantPuntosDeExperiencia());
		this.setCantPuntosDeExperiencia(this.getExpAnterior() +puntosDeExperiencia);
        this.subirDeNivelSiPuede();
	}


	public void subirDeNivelSiPuede(){
		if(this.puedoSubirDeNivel()){
			this.nivel +=1;
			subirDeNivelSiPuede();
		}
	}

	private void subirDeNivel() {
	    this.setNivel(this.getNivel()+1);
	}

	public boolean puedoSubirDeNivel(){
		boolean sePuedeSubirDeNivel = false;
		if(this.estoyEntreElNivel1al10()){
			sePuedeSubirDeNivel = this.getCantPuntosDeExperiencia() >= this.getExpAnterior() + (100 * this.getNivel());
		}else if(this.estoyEntreElNivel11al20()){
			sePuedeSubirDeNivel = this.getCantPuntosDeExperiencia() >= this.getExpAnterior() + (200 * this.getNivel());
		} else if(this.estoyEnElNivel21al50()){
			sePuedeSubirDeNivel = this.getCantPuntosDeExperiencia() >= this.getExpAnterior() + (400 * this.getNivel());
		}else if(this.estoyEnElNivel51EnAdelante()){
			sePuedeSubirDeNivel = this.getCantPuntosDeExperiencia() >= this.getExpAnterior() + (800 * this.getNivel());

		}
		return sePuedeSubirDeNivel;
	}

	private boolean estoyEnElNivel51EnAdelante(){
		return this.getNivel()>= 51;
	}

	private boolean estoyEnElNivel21al50(){	return this.getNivel() >= 21 && this.getNivel() < 51; }

	private boolean estoyEntreElNivel11al20(){ return this.getNivel() >= 11 && this.getNivel() < 21; }

	private boolean estoyEntreElNivel1al10(){ return this.getNivel() >= 1 && this.getNivel() < 11; }


   public void guardarAtributo(Atributo unAtributo){
		try {
			if (this.esUnAtributoDeUnTipoNuevo(unAtributo)) {
				this.atributos.add(unAtributo);
			}
		}catch (RuntimeException e){
			throw new ExceptionAtributo(unAtributo);
		}
	}

	public boolean esUnAtributoDeUnTipoNuevo(Atributo unAtributo) {
		return this.atributos.stream().filter(a-> a.getTipo() == unAtributo.getTipo()).collect(toSet()).isEmpty();
	}

	public boolean contieneEsteAtributo(Atributo atributo){return this.atributos.contains(atributo);}


	public double getDannoArmaDerecha(){
		return this.inventario.getSlotDe(Ubicacion.MANO_DER).getItem().getDannoDelAtributo();
    }
	public double getDannoArmaIzquierda(){
    	return this.inventario.getSlotDe(Ubicacion.MANO_IZQ).getItem().getDannoDelAtributo();
	}

	public double getDañoArmasIzqYDer() {
    	double dannoArmas=  this.getDannoArmaDerecha()+(this.getDannoArmaIzquierda()*0.35);
    	return dannoArmas;
	}

	//TODO: Parametrizar ambos metodos y hacer uno solo porque son iguales
	public double getFuerzaPersonaje(){
    	return this.getAtributo(TipoAtributo.FUERZA).getValor();
	}
	public double getDestrezaPersonaje(){
    	return this.getAtributo(TipoAtributo.DESTREZA).getValor();
	}


	public double getArmadura(){ return this.getAtributo(TipoAtributo.ARMADURA).getValor(); }

	public double getDannoPersonaje(){
    	return this.getDañoArmasIzqYDer()*(this.getFuerzaPersonaje()+(this.getDestrezaPersonaje()/100)/100);
	}
	public double getDefensa(){
		return this.getArmadura()+ (this.getDannoPersonaje()*0.10);
	}

	public double getVida(){
		return this.getAtributo(TipoAtributo.VIDA).getValor();
	}

	public boolean esPersonaje() {
		return true;
	}

	public double atacarAlOponente(Rival rival) {
		//Retorna el danno que le genera al rival que luego se le descuentan en las vidas.
    	double dannoDelAtacante= this.getDannoPersonaje();
    	double defensaRival= rival.getDefensa();
    	double dannoRecibidoRival= dannoDelAtacante - defensaRival;
    	return dannoRecibidoRival;
	}


	public void comprarItem(Item itemAvender, Integer costoDeVenta) {
		try {
			if (this.getBilletera() >= costoDeVenta) {
				this.descontarDinero(costoDeVenta);
				this.guarItemEnMochila(itemAvender);
				this.notificarMisiones(); /** Se notifica a los objetos observadores **/
			}
		}catch (RuntimeException e){ throw new NoHaySuficienteDineroParaComprar(costoDeVenta,this.getBilletera());}
	}

	public void descontarDinero(Integer costoDeVenta) {
		this.billetera = this.billetera - costoDeVenta;
	}

	public void venderItem(Item item, Integer costoDeCompra) {
		this.sacarItemDeMochila(item);
		this.sumarDinero(costoDeCompra);
	}

	private void sacarItemDeMochila(Item item) { this.getMochila().remove(item); }

	private void sumarDinero(Integer costoDeCompra) {
		this.billetera = this.billetera + costoDeCompra;
	}

	public boolean contieneItem(Item item){
		List<Item> allItems = mochila;
		allItems.addAll(this.inventario.getAllItems());
		return allItems.contains(item);
	}

	public RegistroCombate getRegistroCombateContra(Rival rival){
		return this.historialCombates.stream().filter(r -> r.getRival().equals(rival)).collect(Collectors.toList()).get(0);
	}

	public int getCantGanadasContra(Rival rival){
		return this.getRegistroCombateContra(rival).getCantGanadas();
	}

	public boolean estaEnTaberna(){
		return this.getLugar().esTaberna();
	}

	/*********************** MISIONES ********************/

	/** este metodo es el attach del Observer **/
	public void  aceptarMision(Mision misionNueva){
		//El personaje solo puede aceptar misiones en la Tabernaif(estaEnTaberna() ){

			this.misionesAceptadas.add(misionNueva);


	}

	/** este metodo es el detach del Observer **/
	public void eliminarMisionRealizada(Mision mision){
		//Prec.: Mision debe estar aceptada
		this.agregarMisionCompletada(mision);
		this.getMisionesAceptadas().remove(mision);
	}

	public void agregarMisionCompletada(Mision mision){
		this.getMisionesCompletadas().add(mision);
	}

	public int getVecesGanadasContraMonstruo(){
		List<RegistroCombate> historialMonstruos = this.historialCombates.stream().filter(r -> !r.getRival().esPersonaje()).collect(toList());
		return historialMonstruos.stream().filter(r -> r.getCantGanadas()>0).collect(toList()).size();
	}


	public ResultadoCombate combatirContra(Rival rival){
		ResultadoCombate resultado;
		Combate combate = new Combate(this, rival);
		resultado = combate.realizarCombate(this, rival);
		RegistroCombate registro = new RegistroCombate(combate.getResultadoCombate(), rival, this);
		registro.incrementarCantGanadasSiCorresponde();
		this.historialCombates.add(registro);
		this.notificarMisiones(); /** Se notifica a los objetos observadores **/
		if(resultado.getGanador().equals(this)){
			this.combatesGanados++;
		}
		return resultado;
	}

	public void notificarMisiones(){
		this.misionesAceptadas.stream().forEach(m -> m.update(this));
	}

	public void disminuirDeLaBilletera(int dinero){
		this.setBilletera(this.billetera - dinero);
	}

	public void agregarABilletera(int dinero){this.setBilletera(this.billetera + dinero);}

    public List<String> getMisionesCompletadasNombres() {
		List<String>nombreDeLasMisiones = new ArrayList<>();

		for(Mision m: this.getMisionesCompletadas()) {
			nombreDeLasMisiones.add(m.getNombre());

		}
		return nombreDeLasMisiones;
    }
}