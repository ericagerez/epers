package ar.edu.unq.epers.woe.backend.model.eventos;

public class Perdedor extends Ganador{
    public Perdedor(String nombrePJ, String nombreLugar, String nombreContrincante, String claseContrincante, String claseGanador, String razaContrincante, String razaGanador) {
        super(nombrePJ, nombreLugar, nombreContrincante, claseContrincante, claseGanador, razaContrincante, razaGanador);
    }
}
